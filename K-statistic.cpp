/*
Объяснение: как работает FindKStatistic. n - размер входного массива. Будет в цикле изменять массив следующим образом: 
1) Разобьем его случайным пивотом на две части. В первой элементы меньше него, во второй больше. Сохраним их количество в first_half и second_half.
2) Если k меньше first_half, то пройдемся по массиву и перенесем все элементы меньше пивота в начало массива. Теперь первые first_half < n элементов все меньше пивота. Установим новую длину n = first_half и отныне будем работать только с этой частью исходного массива. k не изменяется, так как искомое число все ещё на k-ой позиции этой части массива.
3) Если k больше first_half, то в начало массива перенесем все элементы большие пивота. Установим новую длину n = second_half и отныне будем работать только с этой частью исходного массива. k изменяется на k-first_half, так как мы забываем о first_half элементов меньше пивота.
4) В конце цикла (n=1) в начало массива перенесется искомый элемент.
Доп. память - О(1), т.к. такой подход использует уже выделеные O(n) памяти на хранение исходного массива. Рекурсия заменена циклом.
*/
#include <iostream>
#include <vector>
#include <random>
#include <ctime>
#include <cstdlib>

void partition(std::vector<int>& array, int& k, int& n){
	int random = rand()%n;
	int first_half=0;
	int second_half=0;
	int rnd = array[random];
	for (int i=0; i<n; i++){
		if (array[i]<rnd){
			first_half++;
		} else {
			second_half++;
		}
	}
	int pointer = 0;
	if (k < first_half){
		for (int j=0; j<n; j++){
			if (array[j]<rnd){
				array[pointer]=array[j];
				pointer++;
			}
		}
		n=first_half;
	} else {
		for (int j=0; j<n; j++){
			if (array[j]>=rnd){
				array[pointer]=array[j];
				pointer++;
			}
		}
		n=second_half;
		k-=first_half;
	}
}

int findKStatistic(std::vector<int>& array, int k){
	int n = array.size();
	while (n>1){
		partition(array, k, n);
	}
	return array[0];
}

int main(){	
	int n, k;
	std::cin >> n >> k;
	std::vector<int> array(n);
	for (int i=0; i<n; i++){
		scanf("%d",&array[i]);	
	}
	srand(time(0));
	std::cout << findKStatistic(array,k);
}

