#include <vector>
#include <iostream>
#include <algorithm>
#include <set>

class SegmentTree {
public:
    SegmentTree(std::vector<int> values) : values(values) {
        nodeValues.resize(4 * values.size());
        queries.resize(4 * values.size());
        build(0, 0, values.size());
    }
    void build(int v, int vl, int vr) {
        if (vl == vr - 1) {
            nodeValues[v] = {values[vl]};
        } else {
            int m = (vl + vr) / 2;
            build(2 * v + 1, vl, m);
            build(2 * v + 2, m, vr);
            nodeValues[v].resize(nodeValues[2 * v + 1].size() + nodeValues[2 * v + 2].size());
            std::merge(nodeValues[2 * v + 1].begin(), nodeValues[2 * v + 1].end(), nodeValues[2 * v + 2].begin(), nodeValues[2 * v + 2].end(), nodeValues[v].begin());
        }
    }
    void set(int v, int vl, int vr, int i, int oldValue, int newValue) {
        if (vl == vr - 1) {
            nodeValues[v] = {newValue};
        } else {
            int m = (vl + vr) / 2;
            if (i < m) {
                set(2 * v + 1, vl, m, i, oldValue, newValue);
            } else {
                set(2 * v + 2, m, vr, i, oldValue, newValue);
            }
            queries[v].push_back({-1, oldValue});
            queries[v].push_back({1, newValue});
        }
    }
    void set(int i, int newValue) {
        set(0, 0, values.size(), i, values[i], newValue);
        values[i] = newValue;
    }
    int get(int v, int vl, int vr, int l, int r, int L, int R) {
        if (vl == l && vr == r) {
            if (queries[v].size() > C) {
                std::multiset<int> s(nodeValues[v].begin(), nodeValues[v].end());
                for (auto [type, x]: queries[v]) {
                    if (type == 1)
                        s.insert(x);
                    else
                        s.erase(s.find(x));
                }
                queries[v].clear();
                nodeValues[v] = std::vector<int>(s.begin(), s.end());
            }
            int result = std::upper_bound(nodeValues[v].begin(), nodeValues[v].end(), R) - std::lower_bound(nodeValues[v].begin(), nodeValues[v].end(), L);
            for (auto[type, x]: queries[v]) {
                if (x >= L && x <= R) {
                    if (type == -1) {
                        result--;
                    } else {
                        result++;
                    }
                }
            }
            return result;
        } else {
            int m = (vl + vr) / 2;
            if (r <= m) {
                return get(2 * v + 1, vl, m, l, r, L, R);
            } else if (m <= l) {
                return get(2 * v + 2, m, vr, l, r, L, R);
            } else {
                return get(2 * v + 1, vl, m, l, m, L, R) + get(2 * v + 2, m, vr, m, r, L, R);
            }
        }
    }
private:
    const int C = 400;
    std::vector<int> values;
    std::vector<std::vector<int>> nodeValues;
    std::vector<std::vector<std::pair<int, int>>> queries;
};

int main() {
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);
    std::cout.tie(0);
    int n, m;
    std::cin >> n >> m;
    std::vector<int> v(n);
    for (int i = 0; i < n; ++i) {
        std::cin >> v[i];
    }
    SegmentTree seg(v);
    for (int i = 0; i < m; ++i) {
        std::string s;
        std::cin >> s;
        if (s == "GET") {
            int l, r, L, R;
            std::cin >> l >> r >> L >> R;
            std::cout << seg.get(0, 0, n, l - 1, r, L, R) << '\n';
        } else {
            int i, x;
            std::cin >> i >> x;
            seg.set(i - 1, x);
        }
    }
}
