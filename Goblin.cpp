#include <iostream>
#include <deque>

struct priority_queue{

	std::deque <int> first_half;
	std::deque <int> second_half;
 
	void add_to_end(int x){
		second_half.push_back(x);
		if (first_half.size() < second_half.size()){
			first_half.push_back(second_half.front());
			second_half.pop_front();
		}
	}
 
	void add_to_middle(int x){
		if (first_half.size() <= second_half.size()){
			first_half.push_back(x);
		} else second_half.push_front(x);
	}
 
	int remove_first(){
		int number;
		number = first_half.front();
		first_half.pop_front();
		if (first_half.size() < second_half.size()){
			first_half.push_back(second_half.front());
			second_half.pop_front();
		} return number;
	}
};

int main(){

	int n, number;
	char sign;
	priority_queue queue;
	std::cin >> n;
	for (int i=0; i<n; i++){
		std::cin >> sign;
		if (sign == '+'){
			std::cin >> number;
			queue.add_to_end(number);
		} if (sign == '*'){
			std::cin >> number;
			queue.add_to_middle(number);
		} if (sign == '-'){
			std::cout << queue.remove_first() << '\n' ;
		}
	}
}
