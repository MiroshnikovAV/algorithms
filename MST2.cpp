#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <tuple>

class Graph {
public:
    explicit Graph(int n) : graph(n), sz(n) {}
    std::vector<std::pair<int, int>>& operator[](int i) {
        return graph[i];
    }
    const std::vector<std::pair<int, int>>& operator[](int i) const {
        return graph[i];
    }
    void addEdge(int start, int end, int weight) {
        std::pair<int, int> p = {end, weight};
        graph[start].push_back(p);
    }
    int size() const {
        return sz;
    }
private:
    std::vector<std::vector<std::pair<int, int>>> graph;
    int sz = 0;
};

class DSU {
public:
    DSU(int n) : parents(n) {
        for (int i = 0; i < n; ++i) {
            makeSets(i);
        }
    }
    void makeSets(int x) {
        parents[x] = x;
    }
    void unionSets(int first, int second) {
        int firstParent = findSets(first);
        int secondParent = findSets(second);
        parents[firstParent] = secondParent;
    }
    int findSets(int x) {
        if (parents[x] == x) {
            return x;
        }
        return parents[x] = findSets(parents[x]);
    }
private:
    std::vector<int> parents;
};

long long countMSTWeight(const Graph& graph) {
    std::set<std::tuple<int, int, int>> heap;
    for (int i = 0; i < graph.size(); ++i) {
        for(auto vertex : graph[i]) {
            heap.insert(std::tie(vertex.second, i, vertex.first));
        }
    }
    long long sum = 0;
    DSU dsu(graph.size());
    while(!heap.empty()) {
        auto start = heap.begin();
        if (dsu.findSets(std::get<1>(*start)) != dsu.findSets(std::get<2>(*start))) {
            dsu.unionSets(std::get<1>(*start), std::get<2>(*start));
            sum += std::get<0>(*start);
        }
        heap.erase(start);
    }
    return sum;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    int n, m;
    std::cin >> n >> m;
    Graph graph{n};
    for (int i = 0; i < m; ++i) {
        int start, end, weight;
        std::cin >> start >> end >> weight;
        --end;
        --start;
        graph.addEdge(start, end, weight);
        graph.addEdge(end, start, weight);
    }
    std::cout << countMSTWeight(graph);
}
