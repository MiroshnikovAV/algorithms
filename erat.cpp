#include <iostream>

void FindPrimes(int x){
	bool a[x+1];
	for (int i=0; i<=x; i++){
      		a[i] = false;
	}
	for (int i=2; i<=x; i++){
		if (a[i]==false){
			std::cout << i << " ";
			int k=2;
			while (i*k<=x){
				a[i*k]=true;
				k++;
			}
		}
	}	

}

int main(){

	int n;
	std::cin >> n;
	FindPrimes(n);
}

