#include <iostream>
#include <set>
#include <vector>
#include <algorithm>

bool compare(std::pair<std::pair<long long, long long>, bool> a, std::pair<std::pair<long long, long long>, bool> b){
	if (a.first.first < b.first.first){
		return true;
	} else if (a.first.first == b.first.first && a.first.second < b.first.second) {
		return true;
	} else if (a.first.first == b.first.first && a.first.second == b.first.second && a.second < b.second) {
		return true;
	} return false;
}

int main() {
	long long n, x, y;
	std::cin >> n >> y >> x;
	std::set<long long> traces;
	traces.insert(0);
	traces.insert(x);
	std::multiset<long long> distances;
	distances.insert(x);
	std::vector<std::pair<std::pair<long long, long long>, bool >> pairs;
	for (int i=0; i<n; i++) {
		long long x1, y1, y2;
		std::cin >> x1 >> y1 >> y2;
		pairs.push_back({{y1, x1}, true});
		pairs.push_back({{y2+1, x1}, false});
	}
	std::cout << "f";
	std::sort(pairs.begin(), pairs.end(), compare);
	std::cout << "d";
	long long prev_i = 0;
	/*pairs.push_back({{1000000000,0},0}); 
	int ind=0; 
	for (int i=0;i<=x;i++){
	while (pairs[ind].first==i) {
	if (pairs[ind].second.second==1) {
	long long left=*(traces.lower_bound(pairs[ind].first.second)--);
       long long right=*(traces.lower_bound(pairs[ind].first.second)++);
	distances.insert(right-pairs[ind].first.second); 
	distances.insert(pairs[ind].first.second-left)

	}
	} 
	cout<<*distances.rbegin()<<"\n"; 
	}
	*/
	for(auto i : pairs) {
		if (i.second) {
			traces.insert(i.first.first);
			long long left = *(traces.lower_bound(i.first.second)--);
			long long right = *(traces.lower_bound(i.first.second)++);
			distances.erase(distances.find(right-left));
			distances.insert(right-i.first.second);
			distances.insert(i.first.second-left);	
		} else {
			long long left = *(traces.lower_bound(i.first.second)--);
			long long right = *(traces.lower_bound(i.first.second)++);
			traces.erase(i.first.second);
			distances.erase(distances.find(right-i.first.second));
			distances.erase(distances.find(i.first.second - left));
			distances.insert(right-left);
		}
		if (i.first.first - prev_i) {
			std::cout << *distances.rbegin() << '\n';
		}
		prev_i = i.first.first;
	}
}




