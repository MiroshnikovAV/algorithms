#include <iostream>
#include <vector>
#include <algorithm>

int backPackAlgorithm(int s, const std::vector<int>& items) {
    int n = items.size();
    std::vector<std::vector<int>> dp(n+1, std::vector<int>(s+1, 0));
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= s; j++) {
            dp[i][j] = std::max(dp[i-1][j], j - items[i-1] >= 0 ? dp[i-1][j - items[i-1]] + items[i-1]:0);
        }
    }
    return dp[n][s];
}


int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    int s, n;
    std::cin >> s >> n;
    std::vector<int> items(n);
    for (int i = 0; i < n; i++) {
        std::cin >> items[i];
    }
    std::cout << backPackAlgorithm(s, items);
}
