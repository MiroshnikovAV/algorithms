#include <iostream>
#include <cstring>

class String{
public:
	String() = default;
	String(const char* line);
	String(const size_t& len, const char c);
	String(const String& string);
	String(const char c);
	~String();
	char& front();
	char front() const;
	char& back();
	char back() const;
	size_t length() const;
	bool empty() const;
	void clear();
	void push_back(const char c);
	void pop_back();
	String substr(const int& start, const int& count) const;
	size_t find(const String& string) const;
	size_t rfind(const String& string) const;
	String& operator=(const String& string);
	char& operator[](size_t i);
	char operator[](size_t i) const;
	String& operator+=(const String& string);
private:
	size_t size = 0;
	size_t capacity = 0;
	char* begin = nullptr;
	void swap(String& string);
};
std::istream& operator>>(std::istream& in, String& string) {
	string.clear();
	char c;
	while(in.get(c)) {
		if (c != ' ' && c != '\0' && c != '\n' && c != '\t' && c != '\r') {
			string.push_back(c);
		} else {
			break;
		}
	}
	return in;
}
std::ostream& operator<<(std::ostream& out, const String& string) {
	for(size_t i = 0; i < string.length(); i++) {
		out << string[i];
	}
	return out;
}
String& String::operator +=(const String& string) {
	if (size + string.size >= capacity) {
		if (capacity == 0) {
		       	capacity = 1;
		}
		while (capacity < size + string.size) {
			capacity *= 2;	
		}
		char* temp = new char[capacity];
		memcpy(temp, begin, sizeof(char) * size);
		delete[] begin;
		begin = temp;
	}
	memcpy(begin + size, string.begin, string.size * sizeof(char));
	size += string.size;
	return *this;
}
char String::operator[](size_t i) const {
	return begin[i];
}
char& String::operator[](size_t i) {
	return begin[i];
}
String& String::operator=(const String& string) {
    String temp = string;
    swap(temp);
    return *this;
}
String::String(const char* line) : size(strlen(line)), capacity(size), begin(new char[capacity]) {
	memcpy (begin, line, sizeof(char) * size);
}

String::String(const size_t& len, const char c) : size(len), capacity(size), begin(new char[capacity]) {
	memset(begin, c, sizeof(char) * size);
}

String::String(const String& string) : size(string.length()), capacity(size), begin(new char[size]) {
	memcpy(begin, string.begin, sizeof(char) * size);
}

String::String(const char c) : size(1), capacity(1), begin(new char[1]) {
	begin[0] = c;
}

String::~String() {
	delete[] begin;
}

char& String::front() {
	return *begin;
}

char String::front() const {
	return *begin;
}

char& String::back() {
	return *(begin + size - 1);
}

char String::back() const {
	return *(begin + size - 1);
}

size_t String::length() const {
	return size;
}

bool String::empty() const {
	return size == 0;
}

void String::clear() {
	delete[] begin;
	begin = nullptr;
	size = 0;
	capacity = 0;
}

void String::push_back(const char c) {
	if (capacity == 0) {
		char* string = new char[1];
		capacity = 1;
		begin = string;
	} else if (capacity <= size) {
		capacity *= 2;
		char* string = new char[capacity];
		memcpy(string, begin, sizeof(char) * size);
		delete[] begin;
		begin = string;
	}
	begin[size] = c;
	size++;
}

void String::pop_back() {
	size--;
	if (size <= capacity / 4){
		capacity /= 2;	
		char* string = new char[capacity];
		memcpy(string, begin, sizeof(char) * size);
		delete[] begin;
		begin = string;
	}
}

String operator +(const String& a, const String& b) {
	String string(a);
	string += b;
	return string;
}

bool operator ==(const String& a, const String& b) {
	size_t lenA = a.length(), lenB = b.length();
	if (lenA != lenB){
		return false;
	} else {
		for(size_t i = 0; i < lenA; i++) {
			if (a[i] != b[i]) {
				return false;
			}
		}
		return true;
	}
}

String String::substr(const int& start, const int& count) const {
	String string;
	string.size = count;
	string.capacity = count;
	string.begin = new char[count];
	memcpy(string.begin, begin + start, count * sizeof(char));  
	return string;
}

size_t String::find(const String& string) const {
	size_t ans = size;
	for (size_t i = 0; i < size - string.size + 1; i++) {
		size_t check = 0;
		for (size_t j = 0; j < string.size; j++) {
			if (*(begin + i + j) == *(string.begin + j)) {
				check++;
			} else {
				break;
			}
		}
		if (check == string.size) {
			ans = i;
			break;
		}
	}
	return ans;	
}

size_t String::rfind(const String& string) const {
    size_t ans = size;
    for (size_t i = 0; i < size - string.size + 1; i++) {
        size_t check = 0;
        for (size_t j = 0; j < string.size; j++){
            if (*(begin + i + j) == *(string.begin + j)) {
                check++;
            } else {
                break;
            }
        }
        if (check == string.size) {
            ans = i;
        }
    }
    return ans;
}

void String::swap(String& string) {
	std::swap(size, string.size);
	std::swap(capacity, string.capacity);
	std::swap(begin, string.begin);
}
