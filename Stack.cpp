#include <iostream>
#include <string>

struct Node{
	Node* prev=nullptr;
	int val;
};

struct Stack{
	Node* last = nullptr;
	int size = 0;
	void push(int x){
		size++;
		Node* new_last = new Node();
		new_last->val = x;
		new_last->prev = last;
		last = new_last;
	}

	std::string pop(){
		if (size>0){
			std::string out = std::to_string(last->val);
			Node* timer_delete = last;
			last = last->prev;
			delete timer_delete;
			size--;
			return out;
		} else return "error";
	}

	std::string top(){
		if (size>0){
			return std::to_string(last->val);
		} else return "error";
	}
	void clear(){
		while (size>0){
			Node* timer_delete = last;
			last = last->prev;
			delete timer_delete;
			size--;
		}
	}
};

int main(){
	std::string requests;
	std::cin >> requests;
	Stack stack;
	while (requests!="exit"){
		if (requests == "push"){
			int number;
			std::cin >> number;
			stack.push(number);
			std::cout << "ok"<< '\n';
		}
		if (requests == "back"){
			std::cout << stack.top()<< '\n';
		}
		if (requests == "pop"){
			std::cout << stack.pop() << '\n';
		}
		if (requests == "size"){
			std::cout << stack.size << '\n';
		}
		if (requests == "clear"){
			stack.clear();
			std::cout << "ok" << '\n';
		}
		std::cin >> requests;
	}
	std::cout << "bye"<< '\n';
	return 0;	
}
