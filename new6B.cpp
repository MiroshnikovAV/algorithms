#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <limits>

long long binarySearch(const std::vector<long long>& array, long long n) {
    long long l = 0, r = array.size();
    while (r-l>1) {
        long long m = (l+r)/2;
        if (array[m] >= n) {
            r = m;
        } else {
            l = m;
        }
    }
    return r;
}

std::vector<int> longestIncreasingSubsequence(const std::vector<long long>& sequence) {
    int n = sequence.size();
    const long long inf = std::numeric_limits<long long>::max();
    std::vector<long long> dp(n+1, inf);
    std::vector<int>  prev(n, -1);
    std::vector<int> pos(n, -1);
    dp[0] = -inf;
    for (int i=0; i<n; i++) {
        int index = binarySearch(dp, sequence[i]); 
        dp[index] = sequence[i];
        pos[index] = i;
        prev[i] = pos[index-1];
    }
    std::vector<int> answer;
    int index = binarySearch(dp, inf)-1;
    long long ansIndex = pos[index];
    while (ansIndex != -1) {
       answer.push_back(ansIndex);
       ansIndex = prev[ansIndex];
    }
    return answer;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    int n;
    std::cin >> n;
    std::vector<long long> sequence(n, 0);
    for (int i=0; i<n; i++) {
        std::cin >> sequence[i];
    }
    std::reverse(sequence.begin(), sequence.end());
    std::vector<int> answer = longestIncreasingSubsequence(sequence);
    std::cout << answer.size() << '\n';
    for (int i : answer) {
        std::cout << n-i << ' ';
    }
}

