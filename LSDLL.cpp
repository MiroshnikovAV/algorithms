#include <iostream>
#include <vector>

int getDigit(int k, long long number){
	return (number >> k) & 1;
}

std::vector<long long> radixSort(std::vector<long long> array){
	for (int i=0; i<64; i++){
		std::vector<long long> zero;
		std::vector<long long> one;
		for (int j=0; j<array.size(); j++){
			if (getDigit(i, array[j])){
				one.push_back(array[j]);
			} else {
				zero.push_back(array[j]);
			}
		}
		zero.insert(zero.end(), one.begin(), one.end());
		array = zero;
	}
	return array;
}

int main(){
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(0);
	std::cout.tie(0); 
	int n;
	std::cin >> n;
	std::vector<long long> array(n);
	for(int i=0; i<n; i++){
		std::cin >> array[i];
	}
	array = radixSort(array);
	for(int i=0; i<n; i++){
		std::cout << array[i] << ' ';
	}
}
