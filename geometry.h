#include <iostream>
#include <vector>
#include <cmath>
#include <initializer_list>

bool ifEqual(double first, double second) {
    return fabs(first - second) < 1e-7;
}
bool ifLessOrEqual(double first, double second) {
    return ((first <= second) || (ifEqual(first, second)));
}
struct Point {
    Point() = default;
    Point(double _x, double _y) : x(_x), y(_y){}
    double x = 0;
    double y = 0;
};
bool operator==(const Point& first, const Point& second) {
    return (ifEqual(first.x, second.x) && ifEqual(first.y, second.y));
}
bool operator!=(const Point& first, const Point& second) {
    return !(first == second);
}
double findDistance(const Point& first, const Point& second) {
    return sqrt((second.x - first.x)*(second.x - first.x) + (second.y - first.y)*(second.y - first.y));
}
Point median(const Point& first, const Point& second) {
    return Point((first.x + second.x) / 2, (first.y + second.y) / 2);
}

class Line{
public:
    double x = 1;
    double y = -1;
    double c = 0;
    Line(const Point& first, const Point& second) : x(first.y - second.y), y(second.x - first.x), c(first.x * second.y - second.x * first.y) {}
    Line(const double& k, const double& b) : x(k), y(-1.0), c(b) {}
    Line(const Point& point, const double& k) : x(k), y(-1.0), c(point.y - k*point.x) {}
    Line(const double& _x, const double& _y, const double& _c) : x(_x), y(_y), c(_c){}
    Point evaluate(double coord);
};
Point Line::evaluate(double coord) {
    return Point(coord, (-c-x*coord)/y);
}
bool operator==(const Line& first, const Line& second) {
    return ((ifEqual(first.x*second.y, first.y * second.x)) && (ifEqual(first.y * second.c, first.c * second.y)));
}
bool operator!=(const Line& first, const Line& second) {
    return !(first == second);
}
Point scaleSegment(const Point& first, const Point& second, double scale) {
    Line vector(first, second);
    double coord = first.x + scale*(second.x-first.x);
    return vector.evaluate(coord);
}
Line makeOrthgonal(const Line& line, const Point& point) {
    Line ans(-line.y, line.x, line.y * point.x - line.x * point.y);
    return ans;
}
bool ifParalel(const Line& first, const Line& second) {
    return ifEqual(first.x * second.y - first.y * second.x, 0);
}
Point lineIntersection(const Line& first, const Line& second){
    double det = first.x * second.y - first.y * second.x;
    Point ans((-first.c * second.y + second.c * first.y) / det, (-first.x * second.c + first.c * second.x) / det);
    return ans;
}
Point rotatePoint(const Point& center, const Point& point, const double& angle) {
    return Point(((point.x - center.x) * cos(angle) - (point.y - center.y) * sin(angle)) + center.x, ((point.x - center.x) * sin(angle) + (point.y - center.y) * cos(angle)) + center.y);
}

class Shape{
public:
  virtual double perimeter() const = 0;
  virtual double area() const = 0;
  virtual bool operator==(const Shape& another) const = 0;
  bool operator!=(const Shape& another) const;
  virtual bool isSimilarTo(const Shape& another) const = 0;
  bool isCongruentTo(const Shape& another) const;
  virtual bool containsPoint(const Point& point) const = 0;
  virtual void rotate(const Point& center, const double angle) = 0;
  virtual void reflex(const Point& center) = 0;
  virtual void reflex(const Line& axis) = 0;
  virtual void scale(const Point& center, double coefficient) = 0;
  virtual ~Shape(){}
};

bool Shape::operator!=(const Shape& another) const {
  return (!(*this == another));
}
bool Shape::isCongruentTo(const Shape& another) const {
    return (isSimilarTo(another) && ifEqual(another.area(), area()));
}
bool ifClockwiseRotation(Point first, Point second, Point third) {
    return first.x * (second.y - third.y) + second.x * (third.y - first.y) + third.x * (first.y - second.y) < 0;
}
 
bool ifCounterClockwiseRotation(Point first, Point second, Point third) {
    return first.x * (second.y - third.y) + second.x * (third.y - first.y) + third.x * (first.y - second.y) > 0;
}

class Polygon: public Shape{
public:
    long long verticesCount() const;
    std::vector<Point> getVertices() const;
    bool isConvex() const;
    Polygon() = default;
    Polygon(std::vector<Point> array) : points(array){};
    Polygon(std::initializer_list<Point>& list);
    double perimeter() const;
    double area() const;
    void reflex(const Point& center);
    void reflex(const Line& axis);
    void rotate(const Point& center, const double angle);
    void scale(const Point& center, double coefficient);
    bool containsPoint(const Point& point) const;
    bool operator==(const Shape& another) const;
    bool isSimilarTo(const Shape& another) const;
    virtual ~Polygon() override;
protected:
    std::vector<Point> points;
};

long long Polygon::verticesCount() const {
    return points.size();
}
std::vector<Point> Polygon::getVertices() const {
    return points;
}
bool Polygon::isConvex() const {
    long unsigned int size = points.size();
    int check = 1;
    for (long unsigned int i = 0; i < size; i++) {
        if (!(ifClockwiseRotation(points[i], points[(i+1)%size], points[(i+2)%size]))) {
            check = 0;
            break;
        }
    }
    if (check){
        return true;
    } else { 
        check = 1;
    }
    for (long unsigned int i = 0; i < size; i++) {
        if (!(ifCounterClockwiseRotation(points[i], points[(i+1)%size], points[(i+2)%size]))) {
            check = 0;
            break;
        }
    }
    if (check){
        return true;
    }
    return false;
}
Polygon::Polygon(std::initializer_list<Point>& list) : points(list.size()) {
    int counter = 0;
    for(Point i : list) {
        points[counter] = i;
        counter++;
    }  
}
double Polygon::perimeter() const {
    double ans = 0.0;
    for (long unsigned int i=0; i < points.size(); i++){
        ans += findDistance(points[(i+1) % points.size()], points[i]);
    }
    return ans;
}
double Polygon::area() const {
    long double ans = 0.0;
    long unsigned int size = points.size();
    for (long unsigned int i = 0; i < size; i++) {
        ans += (points[i].y + points[(i+1)%size].y) * (points[(i+1)%size].x - points[i].x) / 2;
    }
    return fabs(ans);
};
void Polygon::reflex(const Point& center){
    rotate(center, acos(-1));
}
void Polygon::reflex(const Line& axis){
    for(Point& vertex : points) {
        Point temp = lineIntersection(makeOrthgonal(axis, vertex), axis);
        vertex = rotatePoint(temp, vertex, acos(-1));
    }
}
void Polygon::rotate(const Point& center, const double angle) {
    for (Point& vertex : points) {
        vertex = rotatePoint(center, vertex, angle*acos(-1)/180);
    }
}
void Polygon::scale(const Point& center, double coefficient) {
    for(Point& vertex : points) {
       vertex = scaleSegment(center, vertex, coefficient); 
    }
}
bool Polygon::containsPoint(const Point& point) const {
    int ans = 0;
    long unsigned int size = points.size();
    Line line(point, 0);
    for (long unsigned int i = 0; i < size; i++) {
        Line temp(points[i], points[(i+1)%size]);
        if (!ifParalel(temp, line)) {
            Point p = lineIntersection(temp, line);
            if (ifLessOrEqual(p.x, point.x)&&((ifLessOrEqual(points[i].x, p.x) && ifLessOrEqual(p.x, points[(i+1)%size].x)) || (ifLessOrEqual(p.x, points[i].x) && ifLessOrEqual(points[(i+1)%size].x, p.x)))) {
                ans += 1;
            }
        }
    }
    if (ans % 2) {
        return true;
    } else {
        return false;
    }
}
bool Polygon::operator==(const Shape& another) const {
    try {
        const Polygon& shape = dynamic_cast<const Polygon&>(another);
        if (shape.points.size() == points.size()) {
                int size = points.size();
                for (int i = 0; i < size; i++) {
                    int check = 1;
                    for (int j = 0; j < size; j++) {
                        if (points[(i + j) % size] != shape.points[j]) {
                            check = 0; 
                            break; 
                        } 
                    }
                    if (check) {
                        return true; 
                    }
                }
                for (int i = 0; i < size; i++) {
                    int check = 1;
                    for (int j = 0; j < size; j++) {
                        if (points[(i - j + size) % size] != shape.points[j]) {
                            check = 0; 
                            break;
                        } 
                    }
                    if (check) {
                        return true;
                    }
                }
            return false;
        }
        return false;
    } catch (const std::bad_cast& e) {
        return false;
    }
}
bool Polygon::isSimilarTo(const Shape& another) const {
    try {
        const Polygon& shape = dynamic_cast<const Polygon&>(another);
            if (points.size() == shape.points.size()) {
                long unsigned int size = points.size();
                for (long unsigned int i = 0; i < size; i++) {
                    int check = 1;
                    long double coefficient = findDistance(points[0], points[1]) / findDistance(shape.points[i], shape.points[(i + 1) % size]); 
                    for (long unsigned int j = 0; j < size; j++) {
                       for (long unsigned int k = 0; k < size; k++) 
                           if (!ifEqual(findDistance(points[j], points[k]), coefficient * findDistance(shape.points[(j + i) % size], shape.points[(k + i) % size]))) {  
                                check = 0;
                                break; 
                            }
                            if (check == 0) {
                                break; 
                            }
                        }
                    if (check) {
                        return true; 
                    }
                }
                for (long unsigned int i = 0; i < size; i++) {
                    int check = 1;
                    long double coefficient = findDistance(points[0], points[1]) / findDistance(shape.points[i], shape.points[(i + size - 1) % size]); 
                     for (long unsigned int j = 0; j < size; j++) {
                        for (long unsigned int k = 0; k < size; k++) 
                        if (!ifEqual(findDistance(points[j], points[k]), coefficient * findDistance(shape.points[(size - j + i) % size], shape.points[(size - k + i) % size]))) { 
                            check = 0; 
                            break;
                        } 
                        if (check == 0) {
                            break;
                        }
                    }
                    if (check) {
                        return true; 
                    }
                }
            }
            return false;
    } catch (const std::bad_cast& e) {
        return false;
    }
}
Polygon::~Polygon(){
    points.clear();
}

class Ellipse: public Shape{
public:
    Ellipse() = default;
    Ellipse(const Point& _firstFocus, const Point& _secondFocus, double sumDistance) : firstFocus(_firstFocus), secondFocus(_secondFocus), distance(sumDistance){}
    Ellipse& operator=(const Ellipse& ellipse);
    Point center() const;
    std::pair<Point,Point> focuses() const;
    std::pair<Line, Line> directrices() const;
    double eccentricity() const;
    virtual double perimeter() const override;
    virtual double area() const override;
    virtual void reflex(const Point& center) override;
    virtual void rotate(const Point& center, const double angle) override;
    virtual void reflex(const Line& axis) override;
    virtual void scale(const Point& center, double coefficient) override;
    bool containsPoint(const Point& point) const;
    bool operator==(const Shape& another) const;
    bool isSimilarTo(const Shape& another) const;
    virtual ~Ellipse(){}
protected:
    Point firstFocus, secondFocus;
    double distance = 0;
};

Ellipse& Ellipse::operator=(const Ellipse& ellipse) {
    firstFocus = ellipse.firstFocus;
    secondFocus = ellipse.secondFocus;
    distance  = ellipse.distance;
    return *this;
}
Point Ellipse::center() const {
    return median(firstFocus, secondFocus);
}
std::pair<Point,Point> Ellipse::focuses() const {
    return {firstFocus, secondFocus};
}
std::pair<Line, Line> Ellipse::directrices() const {
    Line firstDirectrice = makeOrthgonal(Line(firstFocus, secondFocus), scaleSegment(firstFocus, secondFocus, 1+1/eccentricity() + 1/eccentricity()/eccentricity()));
    Line secondDirectrice = makeOrthgonal(Line(firstFocus, secondFocus), scaleSegment(secondFocus, firstFocus, 1+1/eccentricity() + 1/eccentricity()/eccentricity()));
    return {firstDirectrice, secondDirectrice};
}
double Ellipse::eccentricity() const {
    double a = distance / 2;
    double b = sqrt(distance*distance/4 - findDistance(center(), firstFocus)*findDistance(center(), firstFocus));
    return sqrt(1 - b * b / a / a);
}
double Ellipse::perimeter() const {
    double a = distance / 2;
    double b = sqrt(distance*distance/4 - findDistance(center(), firstFocus)*findDistance(center(), firstFocus));
    return acos(-1) * (3 * (a + b) - sqrt((3 * a + b) * (a + 3 * b)));
}
double Ellipse::area() const {
    double a = distance / 2;
    double b = sqrt(distance*distance/4 - findDistance(center(), firstFocus)*findDistance(center(), firstFocus));
    return acos(-1) * a * b;
}
void Ellipse::reflex(const Point& center) {
    rotate(center, acos(-1));
}
void Ellipse::rotate(const Point& center, const double angle) {
    firstFocus = rotatePoint(center, firstFocus, angle*acos(-1)/180);
    secondFocus = rotatePoint(center, secondFocus, angle*acos(-1)/180);
}
void Ellipse::reflex(const Line& axis) {
    Point temp1 = lineIntersection(makeOrthgonal(axis, firstFocus), axis);
    firstFocus = rotatePoint(temp1, firstFocus, acos(-1));
    Point temp2 = lineIntersection(makeOrthgonal(axis, secondFocus), axis);
    secondFocus = rotatePoint(temp2, secondFocus, acos(-1));
}
void Ellipse::scale(const Point& center, double coefficient) {
    firstFocus = scaleSegment(center, firstFocus, coefficient);
    secondFocus = scaleSegment(center, secondFocus, coefficient);
    distance *= coefficient;
}
bool Ellipse::containsPoint(const Point& point) const {
    return ifLessOrEqual(findDistance(point, firstFocus) + findDistance(point, secondFocus), distance);
}
bool Ellipse::operator==(const Shape& another) const {
    try{
        const Ellipse& shape = dynamic_cast<const Ellipse&>(another);
        if ((distance == shape.distance) && (((firstFocus == shape.firstFocus) && (secondFocus == shape.secondFocus)) || ((firstFocus == shape.secondFocus) && (secondFocus == shape.firstFocus)))) {
            return true;
        }
        return false;
    } catch (const std::bad_cast& e) {
        return false;
    }
}
bool Ellipse::isSimilarTo(const Shape& another) const{
    try{
        const Ellipse& shape = dynamic_cast<const Ellipse&>(another);
        if (ifEqual(findDistance(firstFocus, secondFocus) * shape.distance, findDistance(shape.firstFocus, shape.secondFocus) * distance)){
            return true;
        }
        return false;
    } catch (const std::bad_cast& e) {
        return false;
    }
}

class Circle: public Ellipse{
public:
    Circle(const Point& c, double r);
    Circle(const Point& first, const Point& second, const Point& third);
    double radius() const;
    double perimeter() const override;
    double area() const override;
    virtual ~Circle(){}
};

Circle::Circle(const Point& c, double r) {
    firstFocus = c;
    secondFocus = c;
    distance = 2*r;
}
Circle::Circle(const Point& first, const Point& second, const Point& third) {
    double denominator = 2 * (first.x*(second.y - third.y) + second.x*(third.y - first.y) + third.x * (first.y - second.y));
    Point center(((first.x * first.x + first.y * first.y)*(second.y - third.y)+(second.x * second.x + second.y * second.y)*(third.y - first.y)+(third.x * third.x + third.y * third.y)*(first.y - second.y))/denominator, ((first.x * first.x + first.y * first.y)*(second.x - third.x)+(second.x * second.x + second.y * second.y)*(third.x - first.x)+(third.x * third.x + third.y * third.y)*(first.x - second.x))/-denominator);
    firstFocus = center;
    secondFocus = center;
    distance = 2*findDistance(center, first);
}
double Circle::radius() const {
    return distance/2;
}
double Circle::perimeter() const {
    return acos(-1)*radius();
}
double Circle::area() const {
    return radius()*radius()*acos(-1);   
}

class Rectangle: public Polygon{
public:
    Rectangle(const Point& first, const Point& second, const double& coefficient);
    Point center() const;
    std::pair<Line, Line> diagonals() const;
    virtual double area() const override;
    virtual ~Rectangle() override;
};

Rectangle::Rectangle(const Point& first, const Point& second, const double& coefficient) {
    Point center = median(first, second);
    points.resize(4);
    points[0] = first;
    points[1] = rotatePoint(center, second, acos(-1) - 2*atan(coefficient));
    points[2] = second;
    points[3] = scaleSegment(points[1], center, 2);
}
Point Rectangle::center() const {
    return median(points[0], points[2]);
}
std::pair<Line, Line> Rectangle::diagonals() const {
    return {Line(points[0], points[2]), Line(points[1], points[3])};
}
double Rectangle::area() const {
    return findDistance(points[0], points[1]) * findDistance(points[1], points[2]); 
}
Rectangle::~Rectangle(){
    points.clear();
}

class Square: public Rectangle{
public:
    Square(const Point& first, const Point& second) : Rectangle(first, second, 1){}
    Circle circumscribedCircle() const;
    Circle inscribedCircle() const;
    double area() const override;
    virtual ~Square() override;
};

Circle Square::circumscribedCircle() const {
    return Circle(points[0], points[1], points[2]);
};
Circle Square::Square::inscribedCircle() const {
    return Circle(median(points[0], points[1]), median(points[1], points[2]), median(points[2], points[3]));
}
double Square::area() const {
    return findDistance(points[0], points[1]) * findDistance(points[0], points[1]);
}
Square::~Square(){
    points.clear();
}

class Triangle: public Polygon{
public:
    Triangle(const Point& first, const Point& second, const Point& third) : Polygon({first, second, third}){}
    Circle circumscribedCircle() const;
    Circle inscribedCircle() const;
    Point centroid() const;
    Point orthocenter() const;
    Line EulerLine() const;
    Circle ninePointsCircle() const;
    double area() const override;
    virtual ~Triangle() override;
};

Circle Triangle::circumscribedCircle() const {
    Circle ans(points[0], points[1], points[2]);
    return ans;
}
Circle Triangle::inscribedCircle() const {
    Line bisector0(points[0], scaleSegment(points[1], points[2], findDistance(points[0], points[1])/(findDistance(points[0], points[1]) + findDistance(points[0], points[2])))); 
    Line bisector1(points[1], scaleSegment(points[2], points[0], findDistance(points[1], points[2])/(findDistance(points[1], points[2]) + findDistance(points[1], points[0])))); 
    Point c = lineIntersection(bisector0, bisector1);
    double r = findDistance(lineIntersection(makeOrthgonal(Line(points[0], points[1]), c), Line(points[0], points[1])), c);
    return Circle(c,r);
}
Point Triangle::centroid() const {
    Line median0(points[0], median(points[1],points[2]));
    Line median1(points[1], median(points[0], points[2]));
    return lineIntersection(median0, median1);
}
Point Triangle::orthocenter() const {
    Line height0 = makeOrthgonal(Line(points[1], points[2]), points[0]);
    Line height1 = makeOrthgonal(Line(points[0], points[2]), points[1]);
    return lineIntersection(height0, height1);
}
Line Triangle::EulerLine() const {
    return Line(orthocenter(), circumscribedCircle().center());
}
Circle Triangle::ninePointsCircle() const {
    return Circle(median(points[0], points[1]), median(points[1], points[2]), median(points[2], points[0]));
}
double Triangle::area() const {
    Line height = makeOrthgonal(Line(points[1], points[2]), points[0]);
    double h = findDistance(lineIntersection(height, Line(points[1], points[2])), points[0]);
    double a = findDistance(points[1], points[2]);
    return a * h / 2;
}
Triangle::~Triangle(){
    points.clear();
}
