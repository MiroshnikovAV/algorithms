#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <tuple>

class Graph {
public:
    explicit Graph(int n) : graph(n), sz(n) {}
    std::vector<std::pair<int, int>>& operator[](int i) {
        return graph[i];
    }
    const std::vector<std::pair<int, int>>& operator[](int i) const {
        return graph[i];
    }
    void addEdge(int start, int end, int weight) {
        std::pair<int, int> p = {end, weight};
        graph[start].push_back(p);
    }
    int size() const {
        return sz;
    }
private:
    std::vector<std::vector<std::pair<int, int>>> graph;
    int sz = 0;
};

Graph MST(const Graph& graph) {
    Graph resultGraph{graph.size()};
    std::set<std::tuple<int, int, int>> heap;
    int start = 0;
    for (auto vertex : graph[start]) {
        heap.insert(std::tie(vertex.second, start, vertex.first));
    }
    std::vector<bool> used(graph.size(), false);
    used[start] = true;
    while (!heap.empty()) {
        std::set<std::tuple<int, int, int>>::iterator start = heap.begin();
        if (!used[std::get<2>(*start)]) {
            used[std::get<2>(*start)] = true;
            resultGraph.addEdge(std::get<1>(*start), std::get<2>(*start), std::get<0>(*start));
            for (auto vertex : graph[std::get<2>(*start)]) {
                heap.insert(std::tie(vertex.second, std::get<2>(*start), vertex.first));
            }
        }
        heap.erase(start);
    }
    return resultGraph;
}

long long countGraphWeight(const Graph& graph) {
    long long sum = 0;
    for (int i = 0; i < graph.size(); ++i) {
        for (auto vertex : graph[i]) {
            sum += vertex.second;
        }
    }
    return sum;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    int n, m;
    std::cin >> n >> m;
    Graph graph{n};
    for (int i = 0; i < m; ++i) {
        int start, end, weight;
        std::cin >> start >> end >> weight;
        --end;
        --start;
        graph.addEdge(start, end, weight);
        graph.addEdge(end, start, weight);
    }
    Graph min = MST(graph);
    std::cout << countGraphWeight(min);
}

