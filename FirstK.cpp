#include <iostream>
#include <vector>
#include <algorithm>

class Heap{
public:
	Heap(const std::vector<long long>& array);
	long long getMax();
	void extractMax();
	void insert(long long x);
private:
	std::vector<long long> values;
	void siftUp(int i);
	void siftDown(int i);
};

Heap::Heap(const std::vector<long long>& array): values(array){
	for(int i=values.size()-1; i>=0; i--){
		siftDown(i);
	}
}
long long Heap::getMax(){
	return values[0];
}
void Heap::extractMax(){
	values[0] = values[values.size()-1];
	values.pop_back();
	siftDown(0);
}
void Heap::insert(long long x){
        values.push_back(x);
        siftUp(values.size()-1);
}
void Heap::siftUp(int i){
        if (i<=0){
            return;
        }
        long long& parent = values[(i-1)/2];
        long long& branch = values[i];
        if (parent < branch){
                std::swap(parent, branch);
                siftUp((i-1)/2);
        }
}

void Heap::siftDown(int i){
	int leftIndex = 2*i+1, rightIndex = 2*i+2;
	long long& parent = values[i];
	if ((rightIndex) < values.size()){
		int j = values[rightIndex] > values[leftIndex];
		long long& branch = values[leftIndex+j];
		if (parent < branch){
			std::swap(parent, branch);
			siftDown(leftIndex+j);
		}
	} else if (leftIndex < values.size()){
		long long& branch = values[leftIndex];
		if (parent < branch){
			std::swap(parent, branch);
			siftDown(leftIndex);
		}
	}
}
std::vector<long long> getSortedVector(Heap& heap, int k){
	std::vector<long long> array;
	for (int i=0; i<k; i++){
		array.push_back(heap.getMax());
		heap.extractMax();
	}
	return array;
}

int main(){
	int n, k;
	std::cin >> n >> k;
	if (n<k){
		k=n;
	}
	std::vector<long long> array(k);
	for (int i=0; i<k; i++){
		std::cin >> array[i];
	}
	Heap heap(array);
	for (int i=k; i<n; i++){
		long long value;
		std::cin >> value;
		if (value < heap.getMax()){
			heap.extractMax();
			heap.insert(value);
		}
	}
	std::vector<long long> newArray = getSortedVector(heap, k);
	for (int i=k-1; i>=0; i--){
		std::cout << newArray[i] << ' ';
	}
}
