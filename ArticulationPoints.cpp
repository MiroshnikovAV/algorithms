#include <iostream>
#include <vector>
#include <algorithm>
#include <deque>
#include <map>
#include <set>

class Graph {
public:
    explicit Graph(int n) : graph(n), sz(n) {}
    void addEdge(int first, int second) {
        graph[first].push_back(second);
    }
    std::vector<int>& operator[](int i) {
        return graph[i];
    }
    std::vector<int> operator[](int i) const {
        return graph[i];
    }
    int size() const {
        return sz;
    }
private:
    std::vector<std::vector<int>> graph;
    int sz = 0;
};

void pointSearch(const Graph& graph, int vertice, std::vector<bool>& used, std::vector<int>& start, std::vector<int>& ret, std::set<int>& points, int& t, int parent) {
    ++t;
    start[vertice] = t;
    ret[vertice] = start[vertice];
    used[vertice] = true;
    int childrenCounter = 0;
    for (int i : graph[vertice]) {
        if (!used[i]) {
            pointSearch(graph, i, used, start, ret, points, t, vertice);
            ret[vertice] = std::min(ret[vertice], ret[i]);
            if (ret[i] >= start[vertice] && parent != -1) {
                points.insert(vertice + 1);
            }
            ++childrenCounter;
        } else if (i != parent){
            ret[vertice] = std::min(ret[vertice], start[i]);
        }
    }
    if (parent == -1 && childrenCounter > 1) {
        points.insert(vertice + 1);
    }
}

std::set<int> findArticulationPoints(const Graph& graph) {
    std::vector<bool> used(graph.size(), false);
    std::vector<int> start(graph.size());
    std::vector<int> ret(graph.size());
    std::set<int> points;
    int t = 0;
    
    for (int j = 0; j < graph.size(); ++j) {
        if (!used[j]) {
            pointSearch(graph, j, used, start, ret, points, t, -1);
        }
    }
    return points;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    int n, m;
    std::cin >> n >> m;
    Graph graph{n};
    for (int i = 0; i < m; ++i) {
        int first, second;
        std::cin >> first >> second;
        graph.addEdge(first-1, second-1);
        graph.addEdge(second-1, first-1);
    }
    std::set<int> points = findArticulationPoints(graph);
    std::cout << points.size() << '\n';
    for (int i : points) {
        std::cout << i << ' ';
    }
}
