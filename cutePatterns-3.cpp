#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>

std::vector<int> reverse(const std::vector<int>& array) {
    long long size = array.size();
    std::vector<int> ans(size);
    for(int i=0; i<size; ++i) {
        ans[i] = array[size-i-1];
    }
    return ans;
}
class BigInteger{
public:
    BigInteger() = default;
    BigInteger(const long long n) : if_possitive(n>=0), numbers(makeVector(n)){}
    BigInteger(const BigInteger& n) : if_possitive(n.if_possitive), numbers(n.numbers){}
    ~BigInteger();
    BigInteger& operator=(const BigInteger& n);
    void swap(BigInteger& n);
    BigInteger& operator +=(const BigInteger& s);
    BigInteger operator-();
    BigInteger& fixNumber(BigInteger& s);
    BigInteger& operator-=(const BigInteger& s);
    BigInteger& operator*=(const BigInteger& s);
    BigInteger& operator /= (const BigInteger &s);
    BigInteger& operator%=(const BigInteger& s);
    friend std::ostream& operator<<(std::ostream& out, const BigInteger& n);
    friend std::istream& operator>>(std::istream& in, BigInteger& n);
    bool operator<=(const BigInteger& number) const;
    bool operator>=(const BigInteger& n) const;
    bool operator==(const BigInteger& second) const;
    bool operator!=(const BigInteger& n) const;
    bool operator<(const BigInteger& n) const;
    bool operator>(const BigInteger& n) const;
    explicit operator int();
    explicit operator bool();
    BigInteger& operator++();
    BigInteger& operator--();
    BigInteger operator++(int);
    BigInteger operator--(int);
    std::string toString() const;
    bool ifEven() const;
private:
    bool if_possitive = true;
    std::vector<int> numbers;
    std::vector<int> makeVector(long long n);
};

BigInteger::~BigInteger(){
    numbers.clear();
}
BigInteger& BigInteger::operator=(const BigInteger& n) {
    BigInteger temp = n;
    swap(temp);
	return *this;
}
void BigInteger::swap(BigInteger& n) {
    std::swap(if_possitive, n.if_possitive);
    std::swap(numbers, n.numbers);
}
BigInteger BigInteger::operator-() {
    BigInteger negative = *this;
    negative.if_possitive = !if_possitive;
    return fixNumber(negative);
}
BigInteger& BigInteger::operator-=(const BigInteger& s) {
    if (this == &s) {
            return (*this -= BigInteger(s));
        }
	if(if_possitive == s.if_possitive){
		long unsigned int k = 0, l = 0;
		int reminder = 0;
		if ((if_possitive && *this >= s) || (!if_possitive && *this <= s)) {
			while (k < numbers.size() && l < s.numbers.size()) {
				int temp = numbers[k];
				numbers[k] = (numbers[k] - s.numbers[k] - reminder + 10) % 10;
				reminder = temp - s.numbers[k] - reminder < 0;
				k++;
				l++;
			}
			while (k < numbers.size()) { 
				int temp = numbers[k];
				numbers[k] = (numbers[k] - reminder + 10) % 10;
				reminder = temp < reminder;
				if (reminder == 0) {
					break;
				}
				k++;
			}
			return fixNumber(*this);
		} else {
			while (k < numbers.size() && l < s.numbers.size()) {
				int temp = numbers[k];
				numbers[k] = (-numbers[k] + s.numbers[l] - reminder + 10) % 10;
				reminder = (-temp + s.numbers[l] - reminder) < 0;
				k++;
				l++;
			}
			while (l < s.numbers.size()) { 
				int temp = (s.numbers[l] - reminder) < 0; 
				numbers.push_back((s.numbers[l] - reminder + 10) % 10);
				reminder = temp;
				l++;
			}
			if_possitive = !if_possitive;
			return fixNumber(*this);
		}
	} else {
		if_possitive = !if_possitive;
		*this += s;
		if_possitive = !if_possitive;
		return fixNumber(*this);
	}
}

BigInteger& BigInteger::operator*=(const BigInteger& s) {
    if (this == &s) {
            return (*this *= BigInteger(s));
        }
    BigInteger ans = 0;
    for (long unsigned int i = 0; i < numbers.size(); ++i) {
        BigInteger temp;
        for (long unsigned int j = 0; j < i; j++) {
            temp.numbers.push_back(0);
        }
        int reminder = 0;
        for(long unsigned int j = 0; j < s.numbers.size(); ++j) {
            temp.numbers.push_back((numbers[i]*s.numbers[j] + reminder) % 10);
            reminder = (numbers[i]*s.numbers[j] + reminder) / 10;
        }
        if (reminder) {
            temp.numbers.push_back(reminder);
        }
        ans += temp;
    }
    ans.if_possitive = (if_possitive == s.if_possitive);
    *this = ans;
    return fixNumber(*this);
}
std::ostream& operator<<(std::ostream& out, const BigInteger& n) {
    if (!(n.if_possitive)){
        out << '-';
    }
    std::vector<int> reversed = reverse(n.numbers);
    for(long unsigned int i = 0; i < reversed.size(); ++i) {
   	    out << reversed[i];
	}
	return out;
}
std::istream& operator>>(std::istream& in, BigInteger& n) {
	n.numbers.clear();
	n.if_possitive = true;
	char c;
	while(in.get(c)) {
		if (c != ' ' && c != '\0' && c != '\n' && c != '\t' && c != '\r') {
		    if (c=='-'){
		        n.if_possitive = false;
		    } else {
			n.numbers.push_back(c-'0');
		    }
		} else {
			break;
		}
	}
	n.numbers = reverse(n.numbers);
	return in;
}

bool BigInteger::operator<=(const BigInteger& number) const {
    if (if_possitive < number.if_possitive){
        return true;
    } else if (if_possitive > number.if_possitive) {
        return false;
    }
    if (numbers.size() == number.numbers.size()) {
        for(int i = static_cast<int>(numbers.size()) - 1; i >= 0; --i) {
            unsigned int ii = static_cast<unsigned int>(i); 
            if ((if_possitive && numbers[ii] > number.numbers[ii]) || ((!if_possitive && numbers[ii] < number.numbers[ii]))) {
                return false;
            }
            if ((if_possitive && numbers[ii] < number.numbers[ii]) || ((!if_possitive && numbers[ii] > number.numbers[ii]))) {
                return true;
            }
        }
        return true;
    } else if (numbers.size() < number.numbers.size()) {
        return if_possitive;
    } else {
        return !if_possitive;
    }
}
bool BigInteger::operator>=(const BigInteger& n) const {
    return  n <= *(this);
}
bool BigInteger::operator==(const BigInteger& second) const {
    return (*(this) <= second && second <= *(this));
}
bool BigInteger::operator!=(const BigInteger& n) const {
    return !(*(this)==n);
}
bool BigInteger::operator<(const BigInteger& n) const {
    return ( !(*(this)>=n));
}
bool BigInteger::operator>(const BigInteger& n) const {
    return (!(*(this)<=n));
}
BigInteger::operator int() {
    return std::stoi(toString());
}
BigInteger::operator bool() {
    return *this != 0;
}
BigInteger& BigInteger::operator++(){
    return (*this+=1);   
}
BigInteger& BigInteger::operator--(){
    return (*this-=1);
}
BigInteger BigInteger::operator++(int){
    BigInteger ans = *this;
    *this+=1;
    return ans;   
}
BigInteger BigInteger::operator--(int){
    BigInteger ans = *this;
    *this -= 1;
    return ans; 
}
std::string BigInteger::toString() const{
    std::string str;
    if (!if_possitive){
        str += '-';
    }
    std::vector<int> reversed = reverse(numbers);
    for( long unsigned int i=0; i < reversed.size(); ++i) {
        str += std::to_string(reversed[i]);
    }
    return str;
}

BigInteger operator+(const BigInteger& first, const BigInteger& second) {
    BigInteger ans = first;
    ans += second;
    return ans;
}
BigInteger operator-(const BigInteger& first, const BigInteger& second) {
    BigInteger ans = first;
    ans -= second;
    return ans;
}
BigInteger operator*(const BigInteger& first, const BigInteger& second) {
    BigInteger ans = first;
    ans *= second;
    return ans;
}
BigInteger operator/(const BigInteger& first, const BigInteger& second) {
    BigInteger ans = first;
    ans /= second;
    return ans;
}
BigInteger& BigInteger::operator%=(const BigInteger& s) {
    if (this == &s) {
            return (*this %= BigInteger(s));
        }
    *this -= s * (*this / s);
    return *this;
}
BigInteger operator%(const BigInteger& first, const BigInteger& second) {
    BigInteger ans = first;
    ans %= second;
    return ans;
}
BigInteger& BigInteger::operator/=(const BigInteger &s) {
    if (this == &s) {
            return (*this /= BigInteger(s));
        }
    if ((*this < s && if_possitive== true && s.if_possitive==true) || (*this > s && if_possitive==false && s.if_possitive==false)){
        *this = 0;
        return *this;
    }
    BigInteger me = *this; 
    BigInteger ans; 
    std::vector <int> array; 
    BigInteger reminder = 0;
    BigInteger copy = s; 
    if (!copy.if_possitive){
        copy.if_possitive = true;
    } 
    for (long unsigned int i = 0; i < me.numbers.size(); i++) {
        int l = 0, r = 9, v = 9;
        reminder += me.numbers[me.numbers.size() - 1 - i];
        while (l <= r) {
            int mid = (l + r) >> 1;
            if (mid * copy <= reminder) {
                v = mid;
                l = mid + 1;
            } else{
                r = mid - 1; 
            }
        }
        reminder -= v * copy;
        reminder *= 10;
        array.push_back(v);
    }
    
    ans.numbers.clear();
    for (int i = 0; i < static_cast<int> (array.size()); i++) {
        ans.numbers.push_back(array[array.size() - i - 1]);
    }
    int pointer = ans.numbers.size();
    while (pointer > 1 && ans.numbers[pointer - 1] == 0) {
        pointer--; 
        ans.numbers.pop_back(); 
    }
    
    ans.if_possitive = 1; 
    if (me.if_possitive != s.if_possitive) {
        ans.if_possitive = !ans.if_possitive;
    }
    if_possitive = ans.if_possitive; 
    numbers = ans.numbers;  
    return fixNumber(*this);
}
BigInteger& BigInteger::operator+=(const BigInteger& s){
    if (this == &s) {
        return (*this += BigInteger(s));
    }
    if (if_possitive==s.if_possitive) {
        long unsigned int k = 0, t = 0;
        int reminder = 0;
        while (k < numbers.size() && t < s.numbers.size()){
            int temp = numbers[k];
            numbers[k] = ((numbers[k] + s.numbers[t] + reminder) % 10);
            reminder = (temp + s.numbers[t] + reminder) / 10;
            k++;
            t++;
        }
        while (k<numbers.size()){
            int temp = numbers[k];
            numbers[k] = ((numbers[k] + reminder) % 10);
            reminder = (temp + reminder) / 10;
            if (!reminder) {
                break;
            }
            k++;
        }
        while (t<s.numbers.size()){
            numbers.push_back((s.numbers[t] + reminder) % 10);
            reminder = (s.numbers[t] + reminder) / 10;
            t++;
        }
        if (reminder){
            numbers.push_back(reminder);
        }
        return *this;
    } else {
        if_possitive = !if_possitive;
        *this -= s;
        if_possitive = !if_possitive;
        return fixNumber(*this);
    }                                                                            
}
BigInteger& BigInteger::fixNumber(BigInteger& s) {
    while (s.numbers[s.numbers.size()-1] == 0){
        s.numbers.pop_back();
        if (s.numbers.empty()){
            s = 0;
            return s;
        }
    }
    if (s == 0)
        s.if_possitive = true;
    return s;
}
std::vector<int> BigInteger::makeVector(long long n) {
    std::vector<int> temp;
    if(n == 0) {
            return {0};
    }
    if (!if_possitive){
        n*=-1;
    }
    while(n > 0) {
            int t = n % 10;
            temp.push_back(t);
            n /= 10;
    }
    return temp;
}

bool BigInteger::ifEven() const {
    return numbers[0] % 2 == 0;
}

bool check(long long mask, long long nextMask, long long n) {
    for (long long i = 0; i < n-1; i++) {
        if (((mask & 1) + ((mask >> 1) & 1) + (nextMask & 1) + ((nextMask >> 1) & 1)) % 4 == 0) {
            return false;
        }
        mask >>= 1;
        nextMask >>= 1;
    }
    return true;
}

class Matrix {
public:
    Matrix(long long m, long long mo) : matrix(m, std::vector<long long>(m, 0)), sz(m), mod(mo) {}
    std::vector<long long>& operator[](long long i) {
        return matrix[i];
    }
    long long size() const {
        return sz;
    }
    Matrix operator*(Matrix& matrix2) const {
        Matrix ans(sz, mod);
        for (long long i=0; i < sz; i++) {
            for (long long j = 0; j < sz; j++) {
                for (long long k = 0; k < sz; k++) {
                    ans[i][j] += (matrix[i][k] * matrix2[k][j]) % mod;
                    ans[i][j] %= mod;
                }
            }
        }
        return ans;
    }
private:
    std::vector<std::vector<long long>> matrix;
    long long sz = 0;
    long long mod;
};

long long cutePatternsCounter(BigInteger& n, long long m, long long mod) {
    Matrix correct(pow(2,m), mod);
    for (long long i = 0; i < pow(2,m); ++i) {
        for (long long j = 0; j < pow(2,m); ++j) {
            correct[i][j] = check(i, j, m) % mod;
        }
    }
    Matrix result(pow(2, m), mod);
    for (long long i=0; i < pow(2,m); ++i) {
        result[i][i] = 1 % mod;
    }
    --n;
    while(n) {
        if (!n.ifEven()) {
            result = result * correct;
        }
        correct = correct * correct;
        n /= 2;
    }
    long long ans = 0;
    for (long long i=0; i < pow(2,m); ++i) {
        for (long long j = 0; j < pow(2,m); ++j) {
            ans = (ans + result[i][j]) % mod;
        }
    }
    return ans % mod;
}


int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    BigInteger n;
    long long m, mod;
    std::cin >> n >> m >> mod;
    std::cout << cutePatternsCounter(n, m, mod);
}
