#include <iostream>
#include <vector>
#include <algorithm>

std::vector<std::vector<int>> createVector(int size, int innerSize){
	std::vector<std::vector<int>> array(size, std::vector<int>(innerSize));
        for (int i=0; i < size; i++){
                for (int j=0; j < innerSize; j++){
                        std::cin >> array[i][j];
                }
	}
	return array;
}

int binarySearch(std::vector<std::vector<int>>& increase, std::vector<std::vector<int>>& decrease, int i, int j, int l){
	int left = 0, right = l - 1;
	bool ifUpperLeft = increase[i][left] > decrease[j][left], ifUpperRight = increase[i][right] > decrease[j][right]; 
	if (ifUpperLeft == ifUpperRight && !ifUpperRight){
		return right + 1;
	} else if (ifUpperLeft == ifUpperRight && ifUpperRight){
		return left + 1;
	} else { 
		while (right - left > 1){
			int middle = (left + right) / 2;
			bool ifUpperMiddle = increase[i][middle] > decrease[j][middle];
			if (ifUpperMiddle) {
				right = middle;
			} else {
				left = middle;
			}
		}
		if (std::max(increase[i][left], decrease[j][left]) > std::max(increase[i][right], decrease[j][right])){
			return right + 1;
		} else {
			return left + 1;
		}
	}
}

int main(){
	std::ios_base::sync_with_stdio(0);
	std::cin.tie(0);
	std::cout.tie(0);	
	int n, m, l;
	std::cin >> n >> m >> l;
	std::vector<std::vector<int>> increase = createVector(n, l);
	std::vector<std::vector<int>> decrease = createVector(m, l);
	int q;
	std::cin >> q;
	for (int k=0; k<q; k++){
		int i, j;
		std::cin >> i >> j;
		i--;
		j--;
		std::cout << binarySearch(increase, decrease, i, j, l) << '\n';
	}
}
	


