#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>

struct Node {
    Node(const Node& node) : index(node.index), mask(node.mask) {}
    Node(long long i, long long m) : index(i), mask(m) {}
    long long index;
    long long mask;
    Node& operator=(const Node& node) {
        index = node.index;
        mask = node.mask;
        return *this;
    }
};

std::vector<long long> findShortestPath(const std::vector<std::vector<long long>>& pathCost) {
    const long long inf = std::numeric_limits<int>::max();
    int n = pathCost.size();
    std::vector<std::vector<long long>> dp(n, std::vector<long long>(1 << n, inf));
    std::vector<std::vector<Node>> prev(n, std::vector<Node>(1 << n, Node{inf, inf}));
    for (int i = 0; i < n; i++) {
        dp[i][1 << i] = 0;
    }
    for (int mask = 0; mask < (1 << n); ++mask) {
        for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {
                if (!((mask >> k) & 1)) {
                    long long newMask = mask | (1 << k);
                    if (dp[k][newMask] > dp[j][mask] + pathCost[j][k]){
                        dp[k][newMask] = dp[j][mask] + pathCost[j][k];
                        prev[k][newMask] = Node{j, mask};
                    }
                }
            }
        }
    }
    long long path = inf, index, mask = (1 << n)-1, countdown = n;
    for (int i = 0; i < n; i++) {
        if (path > dp[i][mask]) {
            path = dp[i][mask];
            index = i;
        }
    }
    std::vector<long long> ans;
    ans.push_back(path);
    while(countdown) {
        ans.push_back(index+1);
        Node& pr = prev[index][mask];
        index = pr.index;
        mask = pr.mask;
        countdown--;
    }
    return ans;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    int n;
    std::cin >> n;
    std::vector<std::vector<long long>> pathCost(n, std::vector<long long>(n, 0));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            std::cin >> pathCost[i][j];
        }
    }
    std::vector<long long> ans = findShortestPath(pathCost);
    std::cout << ans[0] << '\n';
    for (int i = 1; i < ans.size(); ++i) {
        std::cout << ans[i] << ' ';
    }
}


