#include <iostream>
#include <vector>
#include <deque>

class Graph {
public:
    explicit Graph(int n) : graph(n), sz(n){}
    void addEdge(int first, int second) {
        graph[first].push_back(second);
    }
    std::vector<int>& operator[](int i) {
        return graph[i];
    }
    std::vector<int> operator[](int i) const {
        return graph[i];
    }
    int size() const { 
        return sz;   
    }
private:
    std::vector<std::vector<int>> graph;
    int sz = 0;
};

class CycleSearcher {
public:
    CycleSearcher(const Graph& _graph) : graph(_graph), used(graph.size(), 0) {}
    void cycleSearch(int vertice, std::deque<int>& finalPath);
private:
    Graph graph;
    std::deque<int> tempPath;
    std::vector<int> used;
    bool cycleFound = false;
};

void CycleSearcher::cycleSearch(int vertice, std::deque<int>& finalPath) {
    if ((used[vertice] != 0) | cycleFound) {
        return;
    }
    used[vertice] = 1;
    tempPath.push_back(vertice);
    for (int i : graph[vertice]) {
        if (used[i] == 0) {
            cycleSearch(i, finalPath);
        } else if (used[i] == 1) {
            while(tempPath[0] != i) {
                tempPath.pop_front();
            }
            finalPath = tempPath;
            cycleFound = true;
            break;
        }
    }
    tempPath.pop_back();
    used[vertice] = 2;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    int n, m;
    std::cin >> n >> m;
    Graph graph{n};
    for(int i = 0; i < m; ++i) {
        int first, second;
        std::cin >> first >> second;
        graph.addEdge(first-1, second-1);
    }
    std::deque<int> path;
    CycleSearcher cycleSearcher{graph};
    for (int i = 0; i < n; ++i) {
        cycleSearcher.cycleSearch(i, path);
    }
    if (!path.empty()) {
        std::cout << "YES" << '\n';
        for (int i : path) {
            std::cout << i+1 << ' ';
        }
    } else {
        std::cout << "NO";
    }
}
