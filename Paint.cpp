#include <iostream>
#include <vector>
#include <algorithm>

bool howToSort(std::pair<int, int>& a, std::pair<int, int>& b){
	return a.first < b.first;
}

std::vector<std::pair<int,int>> readVector(int n){
	std::vector<std::pair<int, int>> line;
        for (int i=0; i<n; i++){
                int first, second;
                std::cin >> first >> second;
                line.push_back(std::pair<int, int>{first, 1});
                line.push_back(std::pair<int, int>{second, -1});
        }
	return line;
}

int countLayers(std::vector<std::pair<int,int>> line, int n){
	std::sort(line.begin(), line.end(), howToSort);
        int ans=0, counter=0;
        for (int i=0; i<2*n-1; i++){
                counter+=line[i].second;
                if (counter==1){
                        ans += line[i+1].first-line[i].first;
                }
        }
	return ans;
}

int main(){
	
	int n;
	std::cin >> n;
	std::vector<std::pair<int, int>> line = readVector(n);
	std::cout << countLayers(line, n);
}

