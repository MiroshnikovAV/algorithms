#include <iostream>
#include <vector>
#include <algorithm>
#include <deque>
#include <set>

class Graph {
public:
    explicit Graph(int n) : graph(n), sz(n) {}
    void addEdge(int first, int second) {
        graph[first].push_back(second);
    }
    std::vector<int>& operator[](int i) {
        return graph[i];
    }
    int size() const {
        return sz;
    }
private:
    std::vector<std::vector<int>> graph;
    int sz = 0;
};

void findPath(Graph& graph, int vertice, std::vector<std::vector<bool>>& used, std::vector<std::pair<int, int>>& path) {
    for (int i : graph[vertice]) {
        if (!used[vertice][i]){
            used[vertice][i] = true;
            findPath(graph, i, used, path);
            path.push_back({vertice, i});
        }
    }
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    int n, a;
    std::cin >> n >> a;
    Graph graph{n};
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            int value;
            std::cin >> value;
            if (!value && i != j) {
                graph.addEdge(i, j);
            }
        }
    }
    std::vector<std::pair<int, int>> path;
    std::vector<std::vector<bool>> used(n, std::vector<bool>(n, false));
    findPath(graph, a-1, used, path);
    for (int i = path.size()-1; i>=0; --i) {
        std::cout << path[i].first+1 << ' ' << path[i].second+1 << '\n';
    }
}
