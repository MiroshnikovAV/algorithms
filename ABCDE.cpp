#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>

class Matrix {
public:
    Matrix(long long size, int defaultValue) : matrix(size, std::vector<long long>(size, defaultValue)), sz(size) {}
    std::vector<long long>& operator[](long long i) {
        return matrix[i];
    }
    const std::vector<long long>& operator[](long long i) const {
        return matrix[i];
    }    
    long long size() const {
        return sz;
    }
    Matrix& operator=(const Matrix& otherMatrix) {
    	matrix = otherMatrix.matrix;
    	sz = otherMatrix.sz;
    	return *this;
    }
    Matrix operator*(const Matrix& otherMatrix) const {
        Matrix ans(sz, 0);
        for (long long i=0; i < sz; i++) {
            for (long long j = 0; j < sz; j++) {
                for (long long k = 0; k < sz; k++) {
                    ans[i][j] += (matrix[i][k] * otherMatrix[k][j]) % mod;
                    ans[i][j] %= mod;
                }
            }
        }
        return ans;
    }
private:
    std::vector<std::vector<long long>> matrix;
    long long sz = 0;
    const long long mod = 999999937;
};

long long DNACounter(long long n) {
    const long long mod = 999999937;
    Matrix correct(5, 1);
    correct[2][3] = 0;
    correct[2][4] = 0;
    correct[4][3] = 0;
    correct[4][4] = 0;
    Matrix result(5, 0);
    for (int i = 0; i < 5; i++) {
        result[i][i] = 1;
    }
    --n;
    while(n) {
        if (n & 1) {
            result = result * correct;
        }
        correct = correct * correct;
        n /= 2;
    }
    long long ans = 0;
    for (int i=0; i < 5; ++i) {
        for (int j = 0; j < 5; ++j) {
            ans = (ans + result[i][j]) % mod;
        }
    }
    return ans % mod;
}


int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    long long n;
    std::cin >> n;
    while(n) {
        std::cout << DNACounter(n) << '\n';
        std::cin >> n;
    }
}


