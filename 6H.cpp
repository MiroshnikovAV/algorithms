#include <iostream>
#include <vector>

long long commonIncreasingSubsequence(const std::vector<long long>& first, const std::vector<long long>& second) {
    const int n = first.size(), k = second.size();
    std::vector<std::vector<long long>> dp(n+1, std::vector<long long>(k+1, 0));
    for (int i = 1; i <= n; ++i) {
        long long bestIndex = 0;
        for (int j = 1; j <= k; ++j) {
            dp[i][j] = dp[i-1][j];
            if ((dp[i-1][j] < bestIndex + 1) && (first[i-1] == second[j-1])) {
                dp[i][j] = bestIndex + 1;
            }
            if ((dp[i-1][j] > bestIndex) && (second[j-1] < first[i-1])) {
                bestIndex = dp[i-1][j];
            }
        }
    }
    int maxIndex = 0;
    for (int i = 0; i <= k; ++i) {
        if (dp[n][maxIndex] < dp[n][i]) {
            maxIndex = i;
        }
    }
    return dp[n][maxIndex];
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    int n, k; 
    std::cin >> n >> k;
    std::vector<long long> first(n);
    for (int i = 0; i < n; ++i) {
        std::cin >> first[i];
    }
    std::vector<long long> second(k);
    for (int i = 0; i < k; ++i) {
        std::cin >> second[i];
    }
    std::cout << commonIncreasingSubsequence(first, second);
}
