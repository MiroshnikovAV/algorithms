#include <iostream>
#include <vector>

struct Node {
    	Node(long long val) : value(val) {};
    	long long value;
	Node* left = nullptr;
	Node* right = nullptr;
	unsigned char height = 1;
	long long number = 1;
};

class AVLTree {
public:
	void insert(long long value){
        	if (find(value, root) == nullptr){
       			root = insert(value, root);
	     	}
    	}
	void erase(long long value) {
		if (find(value, root) != nullptr) {
			root = erase(value, root);
		}
	}
	Node* findByOrder(long long index) {
		index = root -> number - index + 1;
		Node* ans = findByOrder(index, root);
		return ans;
	}
	~AVLTree(){
		eraseTree(root);
	}
private:
	Node* root = nullptr;
	Node* insert(long long value, Node* node) {
		if (node == nullptr) {
			return new Node(value);
		}
		if (node->value > value) {
			node->left = insert(value, node->left);
		} else {
			node->right = insert(value, node->right);
		}
		return balance(node);
	}
     	void normalizeHeight(Node* node) {
    		unsigned char l = (node -> left == nullptr)?0:(node -> left -> height);
    		unsigned char r = (node -> right == nullptr)?0:(node -> right -> height);
    		node -> height = std::max(l, r)+1;
       	}
    	void normalizeNumber(Node* node) {
    		long long l = (node -> left == nullptr)?0:(node -> left -> number);
    		long long r = (node -> right == nullptr)?0:(node -> right -> number);
    		node -> number = l + r + 1;
       	}
	void normalize(Node* node) {
		normalizeHeight(node);
		normalizeNumber(node);
	}
	int checkBalance(Node* node) {
    		int l = node -> left == nullptr?0:node->left->height;
    		int r = node -> right == nullptr?0:node->right->height;
    		return r-l;
	}
	Node* rotateLeft(Node* node) {
    		Node* tempRoot = node -> right;
    		node -> right = tempRoot -> left;
    		tempRoot -> left = node;
    		normalize(node);
    		normalize(tempRoot);
    		return tempRoot;
	}
	Node* rotateRight(Node* node) {
    		Node* tempRoot = node -> left;
    		node -> left = tempRoot -> right;
    		tempRoot -> right = node;
    		normalize(node);
    		normalize(tempRoot);
    		return tempRoot;
       	}
    	Node* rotateBigLeft(Node* node) {
    		node -> right = rotateRight(node -> right);
    		return rotateLeft(node);
	}
    	Node* rotateBigRight(Node* node) {
    		node -> left = rotateLeft(node -> left);
    		return rotateRight(node);
	}
    	Node* find(long long value, Node* node) {
    		if (node == nullptr){
    			return nullptr;
    		}
    		if (node -> value == value){
    			return node;
    		}
    		if (node -> value > value){
    			return find(value, node -> left);
    		}
    		return find(value, node -> right);
	}
    	Node* balance(Node* node) {
    		if (node == nullptr) {
    			return nullptr;
    		}
    		normalize(node);
    		if (checkBalance(node) == 2) {
    			if (checkBalance(node -> right) < 0) {
    				return rotateBigLeft(node);
    			} else {
    				return rotateLeft(node);
    			}
    		} if (checkBalance(node) == -2) {
    			if (checkBalance(node -> left) > 0) {
    				return rotateBigRight(node);
    			} else {
    				return rotateRight(node);
    			}
    		}
    		return node;
	}
    	Node* next(long long value, Node* node) {
    		if (node == nullptr) {
    			return nullptr;
    		}
    		if (value < node -> value) {
    			Node* left = next(value, node -> left);
    			return left==nullptr?node:left;
    		} else {
    			return next(value, node -> right);
    		}
	}
	Node* eraseMin(Node* node){
	    	if (node->left!=nullptr){
	    		node->left = eraseMin(node->left);
	    		return balance(node);
		} else {
		    	return node->right;
		}
	}
    	Node* erase(long long value, Node* node){
    		if (node == nullptr) {
    			return nullptr;
    		}
    		if (value < node -> value){
    			node->left = erase(value, node->left);
    			return balance(node);
    		}
    		if (value > node -> value) {
    			node->right =  erase(value, node->right);
    			return balance(node);
    		}
    		Node* right = node -> right;
    		Node* left = node -> left;
    		if (right == nullptr) {
    			return left;
    		}
    		Node* newRoot = next(node->value, right);
    		delete node;
    		newRoot -> right = eraseMin(right);
    		newRoot -> left = left;
    		return balance(newRoot);
	}
	void eraseTree(Node* node) {
		if (node != nullptr) {
			eraseTree(node->left);
			eraseTree(node->right);
			delete node;
		}
	}
	Node* findByOrder(long long index, Node* node) {
    		if (node == nullptr) {
			return nullptr;
	    	}
		long long nodeNumber = (node -> right == nullptr)?node -> number:(node -> number) - (node -> right -> number);
    		if (nodeNumber == index) {
			return node;
	    	}
		if (index < nodeNumber) {
			return findByOrder(index, node->left);
    		}
    		return findByOrder(index-nodeNumber, node -> right);
	}
};

int main() {
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(0);
	std::cout.tie(0);
	int command;
	AVLTree tree;
	int n;
	std::cin >> n;
	for (int i=0; i<n; i++) {
		std::cin >> command;
		long long k;
		std::cin >> k;
		if (command == 1) {
			tree.insert(k);
		} else if (command == 0) {
			Node* ans = tree.findByOrder(k);
			if (ans != nullptr) {
				std::cout << ans -> value << '\n';
			}
		} else if (command == -1) {
			tree.erase(k);
		}
	}
}
