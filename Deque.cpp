#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>

template<typename T>
class Deque {
private:
    template <bool isConst>
    class Iterator;
public:

    using iterator = Iterator<false>;
    using const_iterator = Iterator<true>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    Deque();
    Deque(const Deque& anotherDeque);
    explicit Deque(size_t n);
    Deque(size_t n, const T& value);
    Deque& operator=(const Deque& d);
    ~Deque();;
    size_t size() const;
    T& operator[](const int i);
    const T& operator[](const int i) const;
    void push_back(const T& value);
    void push_front(const T& value);
    void pop_back();
    void pop_front();
    T& at(const size_t i);
    const T& at(const size_t i) const;

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;
    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;
    template <bool isConst>
    void insert(const Iterator<isConst>& it, const T& value);
    template <bool isConst>
    void erase(const Iterator<isConst>& it);

private:

    static const int rowSize = 1000;
    const size_t defaultReserve = 3;
    T** deque;

    size_t capacity = 0;
    size_t sz = 0;
    int startRow = 0;
    int startIndex = 0;

    template <bool isConst>
    class Iterator {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = std::conditional_t<isConst, const T, T>;
        using pointer = std::conditional_t<isConst, const T*, T*>;
        using reference = std::conditional_t<isConst, const T&, T&>;
        using iterator_category = std::random_access_iterator_tag;

        Iterator(T** first, T* second);
        Iterator(const Iterator& it);
        Iterator& operator++();
        Iterator operator++(int);
        Iterator& operator--();
        Iterator operator--(int);
        template <bool isConst2>
        bool operator>=(const Iterator<isConst2>& it) const;
        template <bool isConst2>
        bool operator<=(const Iterator<isConst2>& it) const;
        template <bool isConst2>
        bool operator>(const Iterator<isConst2>& it) const;
        template <bool isConst2>
        bool operator<(const Iterator<isConst2>& it) const;
        template <bool isConst2>
        bool operator==(const Iterator<isConst2>& it) const;
        template <bool isConst2>
        bool operator!=(const Iterator<isConst2>& it) const;

        template <bool isConst2>
        int operator-(const Iterator<isConst2>& it) const;
        Iterator& operator+=(const int n);
        Iterator operator+(const int n) const;
        Iterator& operator-=(const int n);
        Iterator operator-(const int n) const;
        std::conditional_t<isConst, const T&, T&> operator*() const;
        std::conditional_t<isConst, const T*, T*> operator->() const;
        operator Iterator<true>();

    private:
        T** rowPointer;
        T* indexPointer;
    };

    void reserve(size_t n);

    T* index(const size_t i) const;
};

template<typename T>
template<bool isConst>
Deque<T>::Iterator<isConst>::Iterator(T** first, T* second) : rowPointer(first), indexPointer(second) {}

template<typename T>
template<bool isConst>
Deque<T>::Iterator<isConst>::Iterator(const Deque<T>::Iterator<isConst>& it) : rowPointer(it.rowPointer), indexPointer(it.indexPointer) {}

template<typename T>
template<bool isConst>
typename Deque<T>::template Iterator<isConst>& Deque<T>::Iterator<isConst>::operator++() {
    if (indexPointer - *rowPointer == rowSize-1) {
        ++rowPointer;
        indexPointer = *rowPointer;
    } else {
        ++indexPointer;
    }
    return *this;
}

template<typename T>
template<bool isConst>
typename Deque<T>::template Iterator<isConst> Deque<T>::Iterator<isConst>::operator++(int) {
    Iterator temp = *this;
    ++(*this);
    return temp;
}

template<typename T>
template<bool isConst>
typename Deque<T>::template Iterator<isConst>& Deque<T>::Iterator<isConst>::operator--() {
    if (indexPointer == *rowPointer) {
        --rowPointer;
        indexPointer = *rowPointer + rowSize-1;
    } else {
        --indexPointer;
    }
    return *this;
}

template<typename T>
template<bool isConst>
typename Deque<T>::template Iterator<isConst> Deque<T>::Iterator<isConst>::operator--(int) {
    Iterator temp = *this;
    --(*this);
    return temp;
}

template<typename T>
template<bool isConst>
template<bool isConst2>
bool Deque<T>::Iterator<isConst>::operator>=(const Deque::Iterator<isConst2> &it) const {
    return (((*this) > it) || ((*this) == it));
}

template<typename T>
template<bool isConst>
template<bool isConst2>
bool Deque<T>::Iterator<isConst>::operator<=(const Deque::Iterator<isConst2> &it) const {
    return it >= (*this);
}

template<typename T>
template<bool isConst>
template<bool isConst2>
bool Deque<T>::Iterator<isConst>::operator>(const Deque::Iterator<isConst2> &it) const {
    if (rowPointer > it.rowPointer) {
        return true;
    }
    if (indexPointer > it.indexPointer) {
        return true;
    }
    return false;
}

template<typename T>
template<bool isConst>
template<bool isConst2>
bool Deque<T>::Iterator<isConst>::operator<(const Deque::Iterator<isConst2> &it) const {
    return it > (*this);
}

template<typename T>
template<bool isConst>
template<bool isConst2>
bool Deque<T>::Iterator<isConst>::operator==(const Deque::Iterator<isConst2> &it) const {
    return (rowPointer == it.rowPointer && indexPointer == it.indexPointer);
}

template<typename T>
template<bool isConst>
template<bool isConst2>
bool Deque<T>::Iterator<isConst>::operator!=(const Deque::Iterator<isConst2> &it) const {
    return !((*this) == it);
}

template<typename T>
template<bool isConst>
template<bool isConst2>
int Deque<T>::Iterator<isConst>::operator-(const Deque::Iterator<isConst2> &it) const {
    return (rowPointer - it.rowPointer)*rowSize + (indexPointer - it.indexPointer);
}

template<typename T>
template<bool isConst>
typename Deque<T>::template Iterator<isConst>& Deque<T>::Iterator<isConst>::operator+=(const int n) {
    T* oldPointer = *rowPointer;
    rowPointer += (n + (indexPointer - oldPointer)) / rowSize;
    indexPointer = *rowPointer + ((indexPointer - oldPointer) + n) % rowSize;
    return *this;
}

template<typename T>
template<bool isConst>
typename Deque<T>::template Iterator<isConst> Deque<T>::Iterator<isConst>::operator+(const int n) const {
    Iterator temp = *this;
    temp += n;
    return temp;
}

template<typename T>
template<bool isConst>
typename Deque<T>::template Iterator<isConst>& Deque<T>::Iterator<isConst>::operator-=(const int n) {
    T* oldPointer = *rowPointer;
    rowPointer -= (rowSize - 1 + n - (indexPointer - oldPointer)) / rowSize;
    indexPointer = *rowPointer + (((indexPointer - oldPointer) - n) % rowSize + rowSize) % rowSize;
    return *this;
}

template<typename T>
template<bool isConst>
typename Deque<T>::template Iterator<isConst> Deque<T>::Iterator<isConst>::operator-(const int n) const {
    Iterator temp = *this;
    temp -= n;
    return temp;
}

template<typename T>
template<bool isConst>
std::conditional_t<isConst, const T&, T&> Deque<T>::Iterator<isConst>::operator*() const {
    return *indexPointer;
}

template<typename T>
template<bool isConst>
std::conditional_t<isConst, const T*, T*> Deque<T>::Iterator<isConst>::operator->() const {
    return indexPointer;
}

template<typename T>
template<bool isConst>
Deque<T>::Iterator<isConst>::operator Iterator<true>() {
    return Iterator<true>(rowPointer, indexPointer);
}

template<typename T>
Deque<T>::Deque(const Deque& anotherDeque) {
    if (sz != 0 || capacity != 0) {
        this->~Deque();
    }
    capacity = 0;
    sz = 0;
    reserve(anotherDeque.capacity);
    capacity = anotherDeque.capacity;
    sz = anotherDeque.sz;
    startRow = anotherDeque.startRow;
    startIndex = anotherDeque.startIndex;
    for (size_t j = 0; j < sz; ++j) {
        try {
            new(index(j)) T(anotherDeque[j]);
        } catch(...) {
            sz = j+1;
            this->~Deque();
            throw;
        }
    }
}

template<typename T>
Deque<T>::Deque() {
    reserve(defaultReserve);
}

template<typename T>
Deque<T>::Deque(size_t n) {
    reserve(n / rowSize + defaultReserve);
    sz = n;
    for (size_t j = 0; j < sz; ++j) {
        try {
            new(index(j)) T();
        } catch(...) {
            sz = j+1;
            this->~Deque();
            throw;
        }
    }
}

template<typename T>
Deque<T>::Deque(size_t n, const T& value) {
    reserve(n / rowSize + defaultReserve);
    for (size_t i = 0; i < n; ++i) {
        try {
            push_back(value);
        } catch(...) {
            throw;
        }
    }
}

template<typename T>
Deque<T>& Deque<T>::operator=(const Deque& d) {
    Deque temp = d;
    std::swap(capacity, temp.capacity);
    std::swap(sz, temp.sz);
    std::swap(deque, temp.deque);
    std::swap(startRow, temp.startRow);
    std::swap(startIndex, temp.startIndex);
    return (*this);
}

template<typename T>
Deque<T>::~Deque() {
    for (size_t i = 0; i < sz; ++i) {
        index(i)->~T();
    }
    for (size_t i = 0; i < capacity; ++i) {
        delete[] reinterpret_cast<int8_t*>(deque[i]);
    }
    if (capacity != 0) {
        delete[] deque;
    }
}

template<typename T>
size_t Deque<T>::size() const {
    return sz;
}

template<typename T>
T& Deque<T>::operator[](const int i) {
    return *index(i);
}

template<typename T>
const T& Deque<T>::operator[](const int i) const {
    return *index(i);
}

template<typename T>
void Deque<T>::push_back(const T& value) {
    try {
        if (sz == (rowSize*(capacity - startRow) - startIndex)) {
            reserve(capacity * defaultReserve);
        }
    } catch(...) {
        throw;
    }
    *end() = value;
    sz++;
}

template<typename T>
void Deque<T>::push_front(const T& value) {
    if (startRow == 0 && startIndex == 0) {
        reserve(capacity * defaultReserve);
    }
    deque[startRow - (startIndex==0)][((startIndex - 1) % rowSize + rowSize) % rowSize] = value;
    sz++;
    --startIndex;
    if (startIndex == -1) {
        startIndex = rowSize-1;
        --startRow;
    }
}

template<typename T>
void Deque<T>::pop_back() {
    (end()--)->~T();
    sz--;
}

template<typename T>
void Deque<T>::pop_front() {
    begin()->~T();
    sz--;
    ++startIndex;
    if (startIndex==rowSize) {
        startIndex = 0;
        ++startRow;
    }
}

template<typename T>
T& Deque<T>::at(const size_t i) {
    if (i >= size()) {
        throw(std::out_of_range("Out of range"));
    }
    return *index(i);
}

template<typename T>
const T& Deque<T>::at(const size_t i) const {
    if (i >= size()) {
        throw(std::out_of_range("Out of range"));
    }
    return *index(i);
}

template<typename T>
typename Deque<T>::iterator Deque<T>::begin() {
    return iterator(deque + startRow + (startIndex) / rowSize, deque[startRow + (startIndex) / rowSize] + ((startIndex) % rowSize + rowSize) % rowSize);
}

template<typename T>
typename Deque<T>::iterator Deque<T>::end() {
    return iterator(deque + startRow + (sz + startIndex) / rowSize, deque[startRow + (sz + startIndex) / rowSize] + ((startIndex + sz) % rowSize + rowSize) % rowSize);
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::begin() const {
    return cbegin();
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::end() const {
    return cend();
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::cbegin() const {
    return const_iterator(deque + startRow + (startIndex) / rowSize, deque[startRow + (startIndex) / rowSize] + ((startIndex) % rowSize + rowSize) % rowSize);
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::cend() const {
    return const_iterator(deque + startRow + (sz + startIndex) / rowSize, deque[startRow + (sz + startIndex) / rowSize] + ((startIndex + sz) % rowSize + rowSize) % rowSize);
}

template<typename T>
typename Deque<T>::reverse_iterator Deque<T>::rbegin() {
    return std::make_reverse_iterator(end());
}

template<typename T>
typename Deque<T>::reverse_iterator Deque<T>::rend() {
    return std::make_reverse_iterator(begin());
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::rbegin() const {
    return crbegin();
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::rend() const {
    return crend();
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::crbegin() const {
    return std::make_reverse_iterator(cend());
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::crend() const {
    return std::make_reverse_iterator(cbegin());
}

template<typename T>
template<bool isConst>
void Deque<T>::insert(const Deque::Iterator<isConst>& it, const T& value) {
    push_back(value);
    int index = it - begin();
    for (int i = sz-1; i > index; --i) {
        std::swap((*this)[i], (*this)[i-1]);
    }
}

template<typename T>
template<bool isConst>
void Deque<T>::erase(const Deque::Iterator<isConst>& it) {
    size_t index = it - begin();
    for (size_t i = index; i < sz-1; ++i) {
        std::swap((*this)[i], (*this)[i+1]);
    }
    pop_back();
}

template<typename T>
void Deque<T>::reserve(size_t n) {
    if (capacity >= n) {
        return;
    }
    try {
        T** newDeque = new T*[n];
        for (size_t i = 0; i < n; ++i) {
            try {
                if (n/3 <= i && i < n/3 + capacity) {
                    newDeque[i] = deque[i - n/3];
                } else {
                    newDeque[i] = reinterpret_cast<T*>(new int8_t[rowSize * sizeof(T)]);
                }
            } catch(...) {
                for (size_t j = 0; j < i; ++j) {
                    delete[] reinterpret_cast<int8_t*>(newDeque[i]);
                }
                if (capacity != 0) {
                    delete[] newDeque;
                }
                throw;
            }
        }
        deque = newDeque;
        capacity = n;
        startRow = n / 3;
    } catch(...) {throw;}
}

template<typename T>
T* Deque<T>::index(const size_t i) const {
    return deque[startRow + (i + startIndex) / rowSize] + ((startIndex + i) % rowSize + rowSize) % rowSize;
}
