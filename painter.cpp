#include <iostream>
#include <vector>

bool ifCovered(long long w, long long h, long long n) {
    if (w*h > 5*n) {
        return false;
    }
    long long size = 0;
    std::vector<std::vector<long long>> array(w, std::vector<long long>(h, 0));
    for (int i=0; i<n; ++i) {
        long long x, y;
        std::cin >> x >> y;
        x--;
        y--;
        if ((x>=0) && (y>=0) && (x<w) && (y<h) && (1-array[x][y])){
            array[x][y] = 1;
            size++;
        }
        if ((w>x-1) && (x-1>=0) && (h>y) && (y>=0) && (1-array[x-1][y])) {
            array[x-1][y] = 1;
            size++;
        }
        if ((x+1<w) && x+1>=0 && y>=0 && y<h && 1-array[x+1][y]) {
            array[x+1][y] = 1;
            size++;
        }
        if ((y-1>=0) && y-1<h && x<w && x>=0 && 1-array[x][y-1]) {
            array[x][y-1] = 1;
            size++;
        }
        if ((y+1<h) && y+1>=0 && x>=0 && x<w && 1-array[x][y+1]) {
            array[x][y+1] = 1;
            size++;
        }
    }
    return (size == w * h);
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    long long w, h, n;
    std::cin >> w >> h >> n;
    if (ifCovered(w, h, n)) {
        std::cout << "Yes";
    } else {
        std::cout << "No";
    }
}
