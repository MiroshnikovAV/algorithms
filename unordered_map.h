#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

template <typename T, typename Allocator = std::allocator<T>>
        class List {
private:
    template <bool isConst>
    class Iterator;
public:
    using iterator = Iterator<false>;
    using const_iterator = Iterator<true>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    explicit List(const Allocator& _alloc = Allocator());
    List(size_t count, const T& value, const Allocator& _alloc = Allocator());
    List(size_t count, const Allocator& _alloc = Allocator());
    List(const List& list);
    List(List&& list) noexcept;
    ~List();
    List& operator=(const List& list);
    List& operator=(List&& list);
    size_t size() const;
    Allocator get_allocator() const;
    iterator begin();
    const_iterator begin() const;
    iterator end();
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;
    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;

    void insert(const_iterator it, const T& value);
    void erase(const_iterator it);
    void push_back(const T& value);
    template <typename ...Args>
    void emplace_back(Args&&... args);
    void emplace_pair_back(T&& value);
    void push_front(const T& value);
    void pop_back();
    void pop_front();
    void swap(List& second);
private:
    struct Node {
        Node() = default;
        explicit Node(const T& v);
        explicit Node(const T&& v);
        Node(const Node& n);
        Node(Node&& n)  noexcept;
        template<typename ...Args>
        explicit Node(Args&&... args);
        T value;
        Node* prev = nullptr;
        Node* next = nullptr;
    };

    Node* nullNode;
    size_t sz = 0;

    using NodeAlloc = typename std::allocator_traits<Allocator>::template rebind_alloc<Node>;
    using Traits = std::allocator_traits<NodeAlloc>;

    NodeAlloc alloc;

    template <bool isConst>
    class Iterator {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = typename std::conditional<isConst, const T, T>::type;
        using pointer = typename std::conditional<isConst, const T*, T*>::type;
        using reference = typename std::conditional<isConst, const T&, T&>::type;
        using iterator_category = std::bidirectional_iterator_tag;
        friend Iterator<not isConst>;
        friend List;

        explicit Iterator(Node* node);
        Iterator(const Iterator& it);
        Iterator(Iterator&& it)  noexcept;
        Iterator& operator=(const Iterator& it);
        Iterator& operator=(Iterator&& it);
        Iterator& operator++();
        Iterator operator++(int);
        Iterator& operator--();
        Iterator operator--(int);
        template <bool isConst2>
        bool operator==(const Iterator<isConst2>& it) const;
        template <bool isConst2>
        bool operator!=(const Iterator<isConst2>& it) const;
        std::conditional_t<isConst, const T&, T&> operator*() const;
        std::conditional_t<isConst, const T*, T*> operator->() const;
        operator Iterator<true>() const;
    private:
        Node* current;
    };
};

template<typename T, typename Allocator>
template<bool isConst>
List<T, Allocator>::Iterator<isConst>::Iterator(List::Node* node) : current(node) {}

template<typename T, typename Allocator>
template<bool isConst>
List<T, Allocator>::Iterator<isConst>::Iterator(const List<T, Allocator>::Iterator<isConst>& it) : current(it.current) {}

template<typename T, typename Allocator>
template<bool isConst>
typename List<T, Allocator>::template Iterator<isConst>& List<T, Allocator>::Iterator<isConst>::operator++() {
    current = current->next;
    return *this;
}

template<typename T, typename Allocator>
template<bool isConst>
typename List<T, Allocator>::template Iterator<isConst> List<T, Allocator>::Iterator<isConst>::operator++(int) {
    Iterator temp = *this;
    current = current->next;
    return temp;
}

template<typename T, typename Allocator>
template<bool isConst>
typename List<T, Allocator>::template Iterator<isConst>& List<T, Allocator>::Iterator<isConst>::operator--() {
    current = current->prev;
    return *this;
}

template<typename T, typename Allocator>
template<bool isConst>
typename List<T, Allocator>::template Iterator<isConst> List<T, Allocator>::Iterator<isConst>::operator--(int) {
    Iterator temp = *this;
    current = current->prev;
    return temp;
}

template<typename T, typename Allocator>
template<bool isConst>
template<bool isConst2>
bool List<T, Allocator>::Iterator<isConst>::operator==(const List::Iterator<isConst2>& it) const {
    return current == (it.current);
}

template<typename T, typename Allocator>
template<bool isConst>
template<bool isConst2>
bool List<T, Allocator>::Iterator<isConst>::operator!=(const List::Iterator<isConst2>& it) const {
    return current != (it.current);
}

template<typename T, typename Allocator>
template<bool isConst>
std::conditional_t<isConst, const T&, T&> List<T, Allocator>::Iterator<isConst>::operator*() const {
    return current->value;
}

template<typename T, typename Allocator>
template<bool isConst>
std::conditional_t<isConst, const T*, T*> List<T, Allocator>::Iterator<isConst>::operator->() const {
    return &current->value;
}

template<typename T, typename Allocator>
template<bool isConst>
List<T, Allocator>::Iterator<isConst>::operator Iterator<true>() const {
    return Iterator<true>(current);
}

template<typename T, typename Allocator>
template<bool isConst>
List<T, Allocator>::Iterator<isConst>::Iterator(List<T, Allocator>::Iterator<isConst>&& it) noexcept : current(std::move(it.current)) {}

template<typename T, typename Allocator>
template<bool isConst>
typename List<T, Allocator>::template Iterator<isConst>& List<T, Allocator>::Iterator<isConst>::operator=(const List<T, Allocator>::Iterator<isConst> &it) {
    if (this == &it) {return *this;}
    current = it.current;
    return *this;
}

template<typename T, typename Allocator>
template<bool isConst>
typename List<T, Allocator>::template Iterator<isConst>& List<T, Allocator>::Iterator<isConst>::operator=(List<T, Allocator>::Iterator<isConst>&& it) {
    if (this == &it) {return *this;}
    current = it.current;
    it.current = nullptr;
    return *this;
}

template<typename T, typename Allocator>
List<T, Allocator>::Node::Node(const T& v) : value(v) {}

template<typename T, typename Allocator>
List<T, Allocator>::Node::Node(const T &&v) : value(std::move(v)) {}

template<typename T, typename Allocator>
List<T, Allocator>::Node::Node(const List::Node &n) : value(n.value), prev(n.prev), next(n.next) {}

template<typename T, typename Allocator>
List<T, Allocator>::Node::Node(List::Node &&n) noexcept : value(std::move(n.value)), prev(std::move(n.prev)), next(std::move(n.next)) {}

template<typename T, typename Allocator>
template<typename... Args>
List<T, Allocator>::Node::Node(Args &&... args) : value(std::forward<Args>(args)...) {}

template<typename T, typename Allocator>
List<T, Allocator>::List(const Allocator& _alloc) : sz(0), alloc(_alloc) {
    nullNode = Traits::allocate(alloc, 1);
    nullNode->next = nullNode;
    nullNode->prev = nullNode;
}

template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const T& value, const Allocator& _alloc) : sz(0), alloc(_alloc) {
    nullNode = Traits::allocate(alloc, 1);
    nullNode->next = nullNode;
    nullNode->prev = nullNode;
    for (size_t i = 0; i < count; i++) {
        push_back(value);
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const Allocator& _alloc) : sz(0), alloc(_alloc) {
    nullNode = Traits::allocate(alloc, 1);
    nullNode->next = nullNode;
    nullNode->prev = nullNode;
    for (size_t i = 0; i < count; i++) {
        Node* prev = nullNode -> prev;
        Node* newNode = Traits::allocate(alloc, 1);
        Traits::construct(alloc, newNode);
        prev->next = newNode;
        nullNode->prev = newNode;
        newNode->next = nullNode;
        newNode->prev = prev;
        ++sz;
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(const List& list) : sz(0) {
    alloc = Traits::select_on_container_copy_construction(list.alloc);
    nullNode = Traits::allocate(alloc, 1);
    nullNode->next = nullNode;
    nullNode->prev = nullNode;
    List::const_iterator it = list.begin();
    while (it != list.end()) {
        push_back(*it);
        ++it;
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::~List() {
    while (sz > 0) {
        pop_back();
    }
    Traits::deallocate(alloc, nullNode, 1);
}

template<typename T, typename Allocator>
List<T, Allocator>& List<T, Allocator>::operator=(const List& list) {
    if (Traits::propagate_on_container_copy_assignment::value) {
        alloc = list.alloc;
    }
    while(sz > list.size()) {
        pop_back();
    }
    List::iterator first = begin();
    List::const_iterator second = list.begin();
    for (size_t i = 0; i < sz; ++i) {
        (*first) = (*second);
        ++first;
        ++second;
    }
    while(second != list.end()) {
        push_back(*second);
        ++second;
    }
    return *this;
}

template<typename T, typename Allocator>
size_t List<T, Allocator>::size() const {
    return sz;
}

template<typename T, typename Allocator>
Allocator List<T, Allocator>::get_allocator() const {
    return alloc;
}

template<typename T, typename Allocator>
typename List<T, Allocator>::iterator List<T, Allocator>::begin() {
    return iterator(nullNode->next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::begin() const {
    return cbegin();
}

template<typename T, typename Allocator>
typename List<T, Allocator>::iterator List<T, Allocator>::end() {
    return iterator(nullNode);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::end() const {
    return cend();
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::cbegin() const {
    return const_iterator(nullNode->next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::cend() const {
    return const_iterator(nullNode);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::reverse_iterator List<T, Allocator>::rbegin() {
    return std::make_reverse_iterator(end());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::reverse_iterator List<T, Allocator>::rend() {
    return std::make_reverse_iterator(begin());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::rbegin() const {
    return crbegin();
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::rend() const {
    return crend();
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::crbegin() const {
    return std::make_reverse_iterator(cend());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::crend() const {
    return std::make_reverse_iterator(cbegin());
}

template<typename T, typename Allocator>
void List<T, Allocator>::insert(List::const_iterator it, const T& value) {
    Node* prev = it.current -> prev;
    Node* newNode = Traits::allocate(alloc, 1);
    Traits::construct(alloc, newNode, value);
    prev->next = newNode;
    it.current->prev = newNode;
    newNode->next = it.current;
    newNode->prev = prev;
    ++sz;
}

template<typename T, typename Allocator>
void List<T, Allocator>::erase(List::const_iterator it) {
    Node* next = it.current->next;
    Node* prev = it.current->prev;
    prev->next = next;
    next->prev = prev;
    Traits::destroy(alloc, it.current);
    Traits::deallocate(alloc, it.current, 1);
    --sz;
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_back(const T& value) {
    insert(end(), value);
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_front(const T& value) {
    insert(begin(), value);
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_back() {
    erase(--end());
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_front() {
    erase(begin());
}

template <typename T, typename Allocator>
void List<T, Allocator>::swap(List<T, Allocator>& second) {
    std::swap(nullNode, second.nullNode);
    std::swap(sz, second.sz);
    std::swap(alloc, second.alloc);
}

template<typename T, typename Allocator>
List<T, Allocator>::List(List &&list) noexcept : sz(list.size()), alloc(list.alloc) {
    nullNode = Traits::allocate(alloc, 1);
    nullNode->next = nullNode;
    nullNode->prev = nullNode;
    std::swap(nullNode, list.nullNode);
    list.sz = 0;
}

template<typename T, typename Allocator>
List<T, Allocator>& List<T, Allocator>::operator=(List &&list) {
    if (Traits::propagate_on_container_move_assignment::value) {
        alloc = list.alloc;
    }
    while(sz > list.size()) {
        pop_back();
    }
    sz = list.sz;
    std::swap(nullNode, list.nullNode);
    return *this;
}

template<typename T, typename Allocator>
template<typename... Args>
void List<T, Allocator>::emplace_back(Args &&... args) {
    auto it = end();
    Node* prev = it.current -> prev;
    Node* newNode = Traits::allocate(alloc, 1);
    Allocator allocator = alloc;
    std::allocator_traits<Allocator>::construct(allocator, &(newNode->value), std::forward<Args>(args)...);
    prev->next = newNode;
    it.current->prev = newNode;
    newNode->next = it.current;
    newNode->prev = prev;
    ++sz;
}

template<typename T, typename Allocator>
void List<T, Allocator>::emplace_pair_back(T &&value) {
    auto it = end();
    Node* prev = it.current -> prev;
    Node* newNode = Traits::allocate(alloc, 1);
    Allocator allocator = alloc;
    std::allocator_traits<Allocator>::construct(allocator, &(newNode->value),
                                                std::move(const_cast<std::remove_const_t<decltype(value.first)>&&>(value.first)),
                                                std::move(value.second));
    prev->next = newNode;
    it.current->prev = newNode;
    newNode->next = it.current;
    newNode->prev = prev;
    ++sz;
}

template <typename Key, typename Value, typename Hash = std::hash<Key>, typename Equal = std::equal_to<Key>, typename Alloc = std::allocator<std::pair<const Key, Value>>>
class UnorderedMap {
    public:
        using NodeType = std::pair<const Key, Value>;

        UnorderedMap();
        UnorderedMap(const UnorderedMap& map);
        UnorderedMap(UnorderedMap&& map) noexcept;
        ~UnorderedMap() = default;
        UnorderedMap& operator=(const UnorderedMap& map);
        UnorderedMap& operator=(UnorderedMap&& map);
        Value& operator[](const Key& key);
        Value& at(const Key& key);
        const Value& at(const Key& key) const;
        size_t size() const;
        using iterator = typename List<NodeType, Alloc>::iterator;
        using const_iterator = typename List<NodeType, Alloc>::const_iterator;
        iterator begin();
        iterator end();
        const_iterator begin() const;
        const_iterator end() const;
        const_iterator cbegin() const;
        const_iterator cend() const;
        std::pair<iterator, bool> insert(const NodeType& node);
        std::pair<iterator, bool> insert(NodeType&& node);
        template <typename InputIter>
        void insert(const InputIter& first, const InputIter& second);
        template <typename ...Args>
        std::pair<iterator, bool> emplace(Args&&... args);
        void erase(const iterator& iter);
        void erase(const iterator& start, const iterator& last);
        iterator find(const Key& key);
        void reserve(size_t n);
        size_t max_size() const;
        float load_factor() const;
        float max_load_factor() const;
        void max_load_factor(float f);
    private:
        using TableType = std::vector<std::vector<typename List<NodeType, Alloc>::iterator>>;

        TableType table;
        List<NodeType, Alloc> list;
        Hash hash;
        float max_load;
        Equal equal;

        void rehash(size_t newSize);
    };

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
UnorderedMap<Key, Value, Hash, Equal, Alloc>::UnorderedMap(): table(1000), list(), hash(), max_load(0.9), equal() {}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
UnorderedMap<Key, Value, Hash, Equal, Alloc>::UnorderedMap(const UnorderedMap &map): table(map.table.size()), list(map.list), hash(map.hash), max_load(map.max_load), equal(map.equal) {
    for (auto it = begin(); it != end(); ++it) {
        table[hash((*it).first) % table.size()].push_back(it);
    }
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
UnorderedMap<Key, Value, Hash, Equal, Alloc>::UnorderedMap(UnorderedMap &&map) noexcept : table(std::move(map.table)), list(std::move(map.list)), hash(map.hash), max_load(map.max_load), equal(map.equal)  {}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
UnorderedMap<Key, Value, Hash, Equal, Alloc>& UnorderedMap<Key, Value, Hash, Equal, Alloc>::operator=(const UnorderedMap &map) {
    if (&map == this) {return *this;}
    auto temp = map;
    list.swap(temp.list);
    std::swap(table, temp.table);
    std::swap(max_load, temp.max_load);
    std::swap(hash, temp.hash);
    std::swap(equal, temp.equal);
    return *this;
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
UnorderedMap<Key, Value, Hash, Equal, Alloc>& UnorderedMap<Key, Value, Hash, Equal, Alloc>::operator=(UnorderedMap &&map) {
    if (&map == this) {return *this;}
    auto temp = std::move(map);
    list.swap(temp.list);
    std::swap(table, temp.table);
    std::swap(max_load, temp.max_load);
    std::swap(hash, temp.hash);
    std::swap(equal, temp.equal);
    return *this;
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
Value &UnorderedMap<Key, Value, Hash, Equal, Alloc>::operator[](const Key &key) {
    if (load_factor() > max_load_factor()) {
        rehash(2 * table.size());
    }
    auto it = find(key);
    if (it != end()) {
        return (*it).second;
    }
    list.push_back({key,Value()});
    it = --list.end();
    table[hash(key) % table.size()].push_back(it);
    return (*it).second;
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
Value &UnorderedMap<Key, Value, Hash, Equal, Alloc>::at(const Key &key) {
    if (load_factor() > max_load_factor()) {
        rehash(2 * table.size());
    }
    auto it = find(key);
    if (it != end()) {
        return (*it).second;
    }
    throw std::out_of_range("Error");
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
const Value& UnorderedMap<Key, Value, Hash, Equal, Alloc>::at(const Key &key) const {
    auto it = find(key);
    if (it != end()) {
        return (*it).second;
    }
    throw std::out_of_range("Error");
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
size_t UnorderedMap<Key, Value, Hash, Equal, Alloc>::size() const {
    return list.size();
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::iterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::begin() {
    return list.begin();
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::iterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::end() {
    return list.end();
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::const_iterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::begin() const {
    return list.cbegin();
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::const_iterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::end() const {
    return list.cend();
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::const_iterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::cbegin() const {
    return list.cbegin();
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::const_iterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::cend() const {
    return list.cend();
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
std::pair<typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::iterator, bool> UnorderedMap<Key, Value, Hash, Equal, Alloc>::insert(const UnorderedMap::NodeType &node) {
    if (load_factor() > max_load_factor()) {
        rehash(2 * table.size());
    }
    auto it = find(node.first);
    if (it != end()) {
        return {it, false};
    }
    list.emplace_back(node);
    it = --list.end();
    table[hash(node.first) % table.size()].push_back(it);
    return {it, true};
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
std::pair<typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::iterator, bool> UnorderedMap<Key, Value, Hash, Equal, Alloc>::insert(UnorderedMap::NodeType &&node) {
    if (load_factor() > max_load_factor()) {
        rehash(2 * table.size());
    }
    auto it = find(node.first);
    if (it != end()) {
        return {it, false};
    }
    auto h = hash(node.first) % table.size();
    list.emplace_pair_back(std::move(node));
    it = --list.end();
    table[h].push_back(it);
    return {it, true};
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
template<typename InputIter>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::insert(const InputIter &first, const InputIter &second) {
    for (auto iter = first; iter != second; iter++) {
        insert(*iter);
    }
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
template<typename... Args>
std::pair<typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::iterator, bool> UnorderedMap<Key, Value, Hash, Equal, Alloc>::emplace(Args &&... args) {
    if (load_factor() > max_load_factor()) {
        rehash(2 * table.size());
    }
    list.emplace_back(std::forward<Args>(args)...);
    auto it = --list.end();
    table[hash((*it).first) % table.size()].push_back(it);
    return {it, true};
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::erase(const UnorderedMap::iterator &iter) {
    if (iter==end()) {return;}
    auto& inner = table[hash((*iter).first) % table.size()];
    for (auto i = inner.begin(); i != inner.end(); ++i) {
        if (*i == iter) {
            inner.erase(i);
            break;
        }
    }

    list.erase(iter);
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::erase(const UnorderedMap::iterator &start,
                                                         const UnorderedMap::iterator &last) {
    auto iter = start;
    while(iter != last) {
        erase(iter++);
    }
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::iterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::find(const Key &key) {
    auto inner = table[hash(key) % table.size()];
    for (size_t i = 0; i < inner.size(); ++i) {
        if (equal(inner[i]->first, key)) {
            return inner[i];
        }
    }
    return end();
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::reserve(size_t n) {
    rehash(std::ceil(n / max_load_factor()));
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
size_t UnorderedMap<Key, Value, Hash, Equal, Alloc>::max_size() const {
    return std::allocator_traits<Alloc>::max_size(list.get_allocator());
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
float UnorderedMap<Key, Value, Hash, Equal, Alloc>::load_factor() const {
    return size()/table.size();
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
float UnorderedMap<Key, Value, Hash, Equal, Alloc>::max_load_factor() const {
    return max_load;
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::max_load_factor(float f) {
    max_load = f;
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::rehash(size_t newSize) {
    if (newSize <= table.size()) {
        return;
    }
    table.clear();
    table = TableType(newSize);
    for (auto it = begin(); it != end(); it++) {
        table[hash((*it).first) % table.size()].push_back(it);
    }
}