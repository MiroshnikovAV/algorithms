#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <map>

class Sets {
public:
	Sets(long long m) : sets(m+1, std::set<long long>()) {}

	void add(long long value, long long setIndex) {
		sets[setIndex].insert(value);
		mp[value].insert(setIndex);
	}	
	void erase(long long value, long long setIndex) {
		sets[setIndex].erase(value);
		mp[value].erase(setIndex);
	}
	void clear(long long setIndex) {
		for (auto i : sets[setIndex]) {
			mp[i].erase(setIndex);
		}
		sets[setIndex].clear();
	}
	void listSet(long long setIndex) {
		if (sets[setIndex].empty()) {
			std::cout << -1 << '\n';		
		} else {
			for (auto i : sets[setIndex]) {
				std::cout << i << ' '; 
			}
			std::cout << '\n';
		}
	}
	void listSetsOf(long long value) {
		if (mp[value].empty()) {
			std::cout << -1 << '\n';
		} else {
			for (auto i: mp[value]) {
				std::cout << i << ' ';
			}
			std::cout << '\n';
		}
	}
private:
	std::vector<std::set<long long>> sets;
	std::map<long long, std::set<long long>> mp;
};

int main() {
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(0);
	std::cout.tie(0);
	long long n, m, k;
	std::cin >> n >> m >> k;
	Sets set(m);
	for (int i=0; i<k; i++) {
		std::string command;
		std::cin >> command;
		if (command == "ADD") {
			long long e, s;
			std::cin >> e >> s;
			set.add(e, s);
		} else if (command == "DELETE") {
			long long e, s;
			std::cin >> e >> s;
			set.erase(e, s);
		} else if (command == "CLEAR") {
			long long s;
			std::cin >> s;
			set.clear(s);
		} else if (command == "LISTSET") {
			long long s;
			std::cin >> s;
			set.listSet(s);
		} else if (command == "LISTSETSOF") {
			long long e;
			std::cin >> e;
			set.listSetsOf(e);
		}
	}
}
