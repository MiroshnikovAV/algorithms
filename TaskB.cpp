#include <iostream>
#include <stack>
#include <string>

int main(){
	std::stack <char> Stack;
	std::string String;
	std::getline(std::cin, String);
	for (char i: String){
		if (i=='(' || i=='[' || i=='{'){
			Stack.push(i);
		} else {
			if (Stack.empty() || i==')' && Stack.top()!='(' || i==']' && Stack.top()!='[' ||  i=='}' && Stack.top()!='{' ){
				std::cout << "no";
				return 0;
			} else Stack.pop();
		}
	}
	if (!Stack.empty()){
		std::cout << "no";
		return 0;
	} else { std:: cout << "yes";}
}
