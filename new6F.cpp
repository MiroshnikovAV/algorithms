#include <iostream>
#include <vector>
#include <algorithm>

int commonSubsequenceLength(std::vector<int>& first, std::vector<int>& second) {
    int n = first.size(), m = second.size();
    std::vector<std::vector<int>> dp(n+1, std::vector<int>(m+1, 0));
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= m; ++j) {
           dp[i][j] = first[i-1] == second[j-1] ? std::max(1 + dp[i-1][j-1], std::max(dp[i-1][j], dp[i][j-1])) : std::max(dp[i-1][j], dp[i][j-1]);
        }
    }
    return dp[n][m];
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    int n;
    std::cin >> n;
    std::vector<int> first(n);
    for (int i = 0; i < n; ++i) {
        std::cin >> first[i];
    }
    int m;
    std::cin >> m;
    std::vector<int> second(m);
    for (int i = 0; i < m; ++i) {
        std::cin >> second[i];
    }
    std::cout << commonSubsequenceLength(first, second);
}
