#include <iostream>
#include <vector>
#include <string>
 
class Heap{
public:
	Heap(int q) : query(q, 0){}
	void insert(long long x);
	long long getMin();
	void extractMin();
	void decreaseKey(int i,int delta);
private:
	std::vector<std::pair<long long,int>> values;
	std::vector<int> query;
	int counter = 0;
	void siftUp(int i);
	void siftDown(int i);
};
 
void Heap::insert(long long x){
	query[counter] = values.size();
	values.push_back(std::pair<long long,int>(x, counter));
	siftUp(values.size()-1);
	counter++;
}
long long Heap::getMin(){
	query[counter] = -1;
	counter++;
	return values[0].first;
}
void Heap::extractMin(){
	values[0].first = values[values.size()-1].first;   
	values[0].second = values[values.size()-1].second;
	query[values[0].second] = 0;
	values.pop_back();
	siftDown(0);
	query[counter] = -1;
	counter++;
}
void Heap::decreaseKey(int i,int delta){
	i--;
	values[query[i]].first-=delta;
	siftUp(query[i]);
	query[counter] = -1;
	counter++;
}
void Heap::siftUp(int i){
	if (i<=0){
	    return; 
	}
	std::pair<long long,int>& parent = values[(i-1)/2];
	std::pair<long long,int>& branch = values[i];
	if (parent.first > branch.first){
		std::swap(parent, branch);
		std::swap(query[parent.second], query[branch.second]);
		siftUp((i-1)/2);
	}
}
 void Heap::siftDown(int i){
	std::pair<long long,int>& parent = values[i];
	if ((2*i+2)<values.size()){
		int j = values[2*i+2].first<values[2*i+1].first;
            	std::pair<long long,int>& branch = values[2*i+1+j];
                if (parent.first > branch.first) {
			std::swap(parent, branch);
                	std::swap(query[parent.second], query[branch.second]);
               		siftDown(2*i+1+j);
		}
	} else if (2*i+1<values.size()){
		std::pair<long long,int>& branch = values[2*i+1];
		if (parent.first > branch.first) {
              		std::swap(parent, branch);
           		std::swap(query[parent.second], query[branch.second]);
    			siftDown(2*i+1);
		}
	}
}
int main(){
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(0); 
	std::cout.tie(0); 
	int q;
	std::cin >> q;
	Heap heap(q);
	for (int j=0; j<q; ++j){
		std::string command;
		std::cin >> command;
		if (command=="insert"){
			long long value;
			std::cin >> value;
			heap.insert(value);	
		} else if (command=="getMin"){
			std::cout << heap.getMin() << '\n';
		} else if (command=="extractMin"){
			heap.extractMin();
		} else if (command=="decreaseKey"){
			int i, delta;
			std::cin >> i >> delta;
			heap.decreaseKey(i,delta);
		}
	}
}
