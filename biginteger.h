#include <iostream>
#include <string>
#include <vector>

std::vector<int> reverse(const std::vector<int>& array) {
    long long size = array.size();
    std::vector<int> ans(size);
    for(int i=0; i<size; ++i) {
        ans[i] = array[size-i-1];
    }
    return ans;
}
class Rational;
class BigInteger{
public:
    BigInteger() = default;
    BigInteger(const long long n) : if_possitive(n>=0), numbers(makeVector(n)){}
    BigInteger(const BigInteger& n) : if_possitive(n.if_possitive), numbers(n.numbers){}
    ~BigInteger();
    BigInteger& operator=(const BigInteger& n);
    void swap(BigInteger& n);
    BigInteger& operator +=(const BigInteger& s);
    BigInteger operator-();
    BigInteger& fixNumber(BigInteger& s);
    BigInteger& operator-=(const BigInteger& s);
    BigInteger& operator*=(const BigInteger& s);
    BigInteger& operator /= (const BigInteger &s);
    BigInteger& operator%=(const BigInteger& s);
    friend std::ostream& operator<<(std::ostream& out, const BigInteger& n);
    friend std::istream& operator>>(std::istream& in, BigInteger& n);
    bool operator<=(const BigInteger& number) const;
    bool operator>=(const BigInteger& n) const;
    bool operator==(const BigInteger& second) const;
    bool operator!=(const BigInteger& n) const;
    bool operator<(const BigInteger& n) const;
    bool operator>(const BigInteger& n) const;
    explicit operator int();
    explicit operator bool();
    BigInteger& operator++();
    BigInteger& operator--();
    BigInteger operator++(int);
    BigInteger operator--(int);
    std::string toString() const;
    friend Rational;
private:
    bool if_possitive = true;
    std::vector<int> numbers;
    std::vector<int> makeVector(long long n);
};

BigInteger::~BigInteger(){
    numbers.clear();
}
BigInteger& BigInteger::operator=(const BigInteger& n) {
    BigInteger temp = n;
    swap(temp);
	return *this;
}
void BigInteger::swap(BigInteger& n) {
    std::swap(if_possitive, n.if_possitive);
    std::swap(numbers, n.numbers);
}
BigInteger BigInteger::operator-() {
    BigInteger negative = *this;
    negative.if_possitive = !if_possitive;
    return fixNumber(negative);
}
BigInteger& BigInteger::operator-=(const BigInteger& s) {
    if (this == &s) {
            return (*this -= BigInteger(s));
        }
	if(if_possitive == s.if_possitive){
		long unsigned int k = 0, l = 0;
		int reminder = 0;
		if ((if_possitive && *this >= s) || (!if_possitive && *this <= s)) {
			while (k < numbers.size() && l < s.numbers.size()) {
				int temp = numbers[k];
				numbers[k] = (numbers[k] - s.numbers[k] - reminder + 10) % 10;
				reminder = temp - s.numbers[k] - reminder < 0;
				k++;
				l++;
			}
			while (k < numbers.size()) { 
				int temp = numbers[k];
				numbers[k] = (numbers[k] - reminder + 10) % 10;
				reminder = temp < reminder;
				if (reminder == 0) {
					break;
				}
				k++;
			}
			return fixNumber(*this);
		} else {
			while (k < numbers.size() && l < s.numbers.size()) {
				int temp = numbers[k];
				numbers[k] = (-numbers[k] + s.numbers[l] - reminder + 10) % 10;
				reminder = (-temp + s.numbers[l] - reminder) < 0;
				k++;
				l++;
			}
			while (l < s.numbers.size()) { 
				int temp = (s.numbers[l] - reminder) < 0; 
				numbers.push_back((s.numbers[l] - reminder + 10) % 10);
				reminder = temp;
				l++;
			}
			if_possitive = !if_possitive;
			return fixNumber(*this);
		}
	} else {
		if_possitive = !if_possitive;
		*this += s;
		if_possitive = !if_possitive;
		return fixNumber(*this);
	}
}

BigInteger& BigInteger::operator*=(const BigInteger& s) {
    if (this == &s) {
            return (*this *= BigInteger(s));
        }
    BigInteger ans = 0;
    for (long unsigned int i = 0; i < numbers.size(); ++i) {
        BigInteger temp;
        for (long unsigned int j = 0; j < i; j++) {
            temp.numbers.push_back(0);
        }
        int reminder = 0;
        for(long unsigned int j = 0; j < s.numbers.size(); ++j) {
            temp.numbers.push_back((numbers[i]*s.numbers[j] + reminder) % 10);
            reminder = (numbers[i]*s.numbers[j] + reminder) / 10;
        }
        if (reminder) {
            temp.numbers.push_back(reminder);
        }
        ans += temp;
    }
    ans.if_possitive = (if_possitive == s.if_possitive);
    *this = ans;
    return fixNumber(*this);
}
std::ostream& operator<<(std::ostream& out, const BigInteger& n) {
    if (!(n.if_possitive)){
        out << '-';
    }
    std::vector<int> reversed = reverse(n.numbers);
    for(long unsigned int i = 0; i < reversed.size(); ++i) {
   	    out << reversed[i];
	}
	return out;
}
std::istream& operator>>(std::istream& in, BigInteger& n) {
	n.numbers.clear();
	n.if_possitive = true;
	char c;
	while(in.get(c)) {
		if (c != ' ' && c != '\0' && c != '\n' && c != '\t' && c != '\r') {
		    if (c=='-'){
		        n.if_possitive = false;
		    } else {
			n.numbers.push_back(c-'0');
		    }
		} else {
			break;
		}
	}
	n.numbers = reverse(n.numbers);
	return in;
}

bool BigInteger::operator<=(const BigInteger& number) const {
    if (if_possitive < number.if_possitive){
        return true;
    } else if (if_possitive > number.if_possitive) {
        return false;
    }
    if (numbers.size() == number.numbers.size()) {
        for(int i = static_cast<int>(numbers.size()) - 1; i >= 0; --i) {
            unsigned int ii = static_cast<unsigned int>(i); 
            if ((if_possitive && numbers[ii] > number.numbers[ii]) || ((!if_possitive && numbers[ii] < number.numbers[ii]))) {
                return false;
            }
            if ((if_possitive && numbers[ii] < number.numbers[ii]) || ((!if_possitive && numbers[ii] > number.numbers[ii]))) {
                return true;
            }
        }
        return true;
    } else if (numbers.size() < number.numbers.size()) {
        return if_possitive;
    } else {
        return !if_possitive;
    }
}
bool BigInteger::operator>=(const BigInteger& n) const {
    return  n <= *(this);
}
bool BigInteger::operator==(const BigInteger& second) const {
    return (*(this) <= second && second <= *(this));
}
bool BigInteger::operator!=(const BigInteger& n) const {
    return !(*(this)==n);
}
bool BigInteger::operator<(const BigInteger& n) const {
    return ( !(*(this)>=n));
}
bool BigInteger::operator>(const BigInteger& n) const {
    return (!(*(this)<=n));
}
BigInteger::operator int() {
    return std::stoi(toString());
}
BigInteger::operator bool() {
    return (numbers[0]==0)?false:true;
}
BigInteger& BigInteger::operator++(){
    return (*this+=1);   
}
BigInteger& BigInteger::operator--(){
    return (*this-=1);
}
BigInteger BigInteger::operator++(int){
    BigInteger ans = *this;
    *this+=1;
    return ans;   
}
BigInteger BigInteger::operator--(int){
    BigInteger ans = *this;
    *this -= 1;
    return ans; 
}
std::string BigInteger::toString() const{
    std::string str;
    if (!if_possitive){
        str += '-';
    }
    std::vector<int> reversed = reverse(numbers);
    for( long unsigned int i=0; i < reversed.size(); ++i) {
        str += std::to_string(reversed[i]);
    }
    return str;
}

BigInteger operator+(const BigInteger& first, const BigInteger& second) {
    BigInteger ans = first;
    ans += second;
    return ans;
}
BigInteger operator-(const BigInteger& first, const BigInteger& second) {
    BigInteger ans = first;
    ans -= second;
    return ans;
}
BigInteger operator*(const BigInteger& first, const BigInteger& second) {
    BigInteger ans = first;
    ans *= second;
    return ans;
}
BigInteger operator/(const BigInteger& first, const BigInteger& second) {
    BigInteger ans = first;
    ans /= second;
    return ans;
}
BigInteger& BigInteger::operator%=(const BigInteger& s) {
    if (this == &s) {
            return (*this %= BigInteger(s));
        }
    *this -= s * (*this / s);
    return *this;
}
BigInteger operator%(const BigInteger& first, const BigInteger& second) {
    BigInteger ans = first;
    ans %= second;
    return ans;
}
BigInteger& BigInteger::operator/=(const BigInteger &s) {
    if (this == &s) {
            return (*this /= BigInteger(s));
        }
    if ((*this < s && if_possitive== true && s.if_possitive==true) || (*this > s && if_possitive==false && s.if_possitive==false)){
        *this = 0;
        return *this;
    }
    BigInteger me = *this; 
    BigInteger ans; 
    std::vector <int> array; 
    BigInteger reminder = 0;
    BigInteger copy = s; 
    if (!copy.if_possitive){
        copy.if_possitive = true;
    } 
    for (long unsigned int i = 0; i < me.numbers.size(); i++) {
        int l = 0, r = 9, v = 9;
        reminder += me.numbers[me.numbers.size() - 1 - i];
        while (l <= r) {
            int mid = (l + r) >> 1;
            if (mid * copy <= reminder) {
                v = mid;
                l = mid + 1;
            } else{
                r = mid - 1; 
            }
        }
        reminder -= v * copy;
        reminder *= 10;
        array.push_back(v);
    }
    
    ans.numbers.clear();
    for (int i = 0; i < static_cast<int> (array.size()); i++) {
        ans.numbers.push_back(array[array.size() - i - 1]);
    }
    int pointer = ans.numbers.size();
    while (pointer > 1 && ans.numbers[pointer - 1] == 0) {
        pointer--; 
        ans.numbers.pop_back(); 
    }
    
    ans.if_possitive = 1; 
    if (me.if_possitive != s.if_possitive) {
        ans.if_possitive = !ans.if_possitive;
    }
    if_possitive = ans.if_possitive; 
    numbers = ans.numbers;  
    return fixNumber(*this);
}
BigInteger& BigInteger::operator+=(const BigInteger& s){
    if (this == &s) {
        return (*this += BigInteger(s));
    }
    if (if_possitive==s.if_possitive) {
        long unsigned int k = 0, t = 0;
        int reminder = 0;
        while (k < numbers.size() && t < s.numbers.size()){
            int temp = numbers[k];
            numbers[k] = ((numbers[k] + s.numbers[t] + reminder) % 10);
            reminder = (temp + s.numbers[t] + reminder) / 10;
            k++;
            t++;
        }
        while (k<numbers.size()){
            int temp = numbers[k];
            numbers[k] = ((numbers[k] + reminder) % 10);
            reminder = (temp + reminder) / 10;
            if (!reminder) {
                break;
            }
            k++;
        }
        while (t<s.numbers.size()){
            numbers.push_back((s.numbers[t] + reminder) % 10);
            reminder = (s.numbers[t] + reminder) / 10;
            t++;
        }
        if (reminder){
            numbers.push_back(reminder);
        }
        return *this;
    } else {
        if_possitive = !if_possitive;
        *this -= s;
        if_possitive = !if_possitive;
        return fixNumber(*this);
    }                                                                            
}
BigInteger& BigInteger::fixNumber(BigInteger& s) {
    while (s.numbers[s.numbers.size()-1] == 0){
        s.numbers.pop_back();
        if (s.numbers.empty()){
            s = 0;
            return s;
        }
    }
    if (s == 0)
        s.if_possitive = true;
    return s;
}
std::vector<int> BigInteger::makeVector(long long n) {
    std::vector<int> temp;
    if(n == 0) {
            return {0};
    }
    if (!if_possitive){
        n*=-1;
    }
    while(n > 0) {
            int t = n % 10;
            temp.push_back(t);
            n /= 10;
    }
    return temp;
}

BigInteger gcd(BigInteger first, BigInteger second){
    if (second == 0) {
        return first;
    } else {
        return gcd(second, first % second);
    }
}

class Rational {
public:
    Rational() = default;
    Rational(const BigInteger& n) : numerator(n), denominator(1) {}
    Rational(const long long n) : numerator(n), denominator(1) {}
    Rational(const Rational& r) : numerator(r.numerator), denominator(r.denominator) {}
    ~Rational() = default;
    bool sign() const;
    bool operator<=(const Rational& r) const;
    bool operator>=(const Rational& r) const;
    bool operator==(const Rational& r) const;
    bool operator!=(const Rational& r) const;
    bool operator<(const Rational& r) const;
    bool operator>(const Rational& r) const;
    Rational operator-() const;
    Rational& operator+=(const Rational& number);
    Rational& operator-=(const Rational& number);
    Rational& operator*=(const Rational& number);
    Rational& operator/=(const Rational& number);
    std::string asDecimal(long unsigned int precision = 0) const;
    explicit operator double();
    Rational& operator=(const Rational& n);
    void swap(Rational& n);
    std::string toString() const;
private:
    BigInteger numerator = 0;
    BigInteger denominator = 1;
};

bool Rational::sign() const {
    return ((numerator.if_possitive == denominator.if_possitive) || numerator == 0);
}
bool Rational::operator<=(const Rational& r) const {
    return ((denominator.if_possitive?numerator:-1*numerator) * (r.denominator.if_possitive?r.denominator:-1*r.denominator) <= (r.denominator.if_possitive?r.numerator:-1*r.numerator) * (denominator.if_possitive?denominator:-1*denominator));
}
bool Rational::operator>=(const Rational& r) const {
    return  r <= *(this);
}
bool Rational::operator==(const Rational& r) const {
    return (*(this) <= r && r <= *(this));
}
bool Rational::operator!=(const Rational& r) const {
    return !(*(this)==r);
}
bool Rational::operator<(const Rational& r) const {
    return !(*(this)>=r);
}
bool Rational::operator>(const Rational& r) const {
    return !(*(this)<=r);
}
Rational Rational::operator-() const {
    if (*this == 0) {
        return *this;
    }
    Rational negative = *this;
    negative.numerator.if_possitive = !negative.numerator.if_possitive;
    return negative;
}

Rational& Rational::operator+=(const Rational& number) {
    if (this == &number) {
        return (*this += Rational(number));
    }
    numerator = numerator * number.denominator + denominator * number.numerator;
    denominator = denominator * number.denominator;
    BigInteger g = gcd(numerator, denominator);
    numerator /= g;
    denominator /= g;
    return *this;
}
Rational& Rational::operator-=(const Rational& number) {
    if (this == &number) {
        return (*this -= Rational(number));
    }
    numerator = numerator * number.denominator - denominator * number.numerator;
    denominator = denominator * number.denominator;
    BigInteger g = gcd(numerator, denominator);
    numerator /= g;
    denominator /= g;
    return *this;
}
Rational& Rational::operator*=(const Rational& number) {
    if (this == &number) {
        return (*this *= Rational(number));
    }
    numerator = numerator * number.numerator;
    denominator = denominator * number.denominator;
    BigInteger g = gcd(numerator, denominator);
    numerator /= g;
    denominator /= g;
    return *this;
}
Rational& Rational::operator/=(const Rational& number) {
    if (this == &number) {
        return (*this /= Rational(number));
    }
    numerator = numerator * number.denominator;
    denominator = denominator * number.numerator;
    BigInteger g = gcd(numerator, denominator);
    numerator /= g;
    denominator /= g;
    return *this;
}
std::string Rational::asDecimal(long unsigned int precision) const {
    std::string ans;
    if (!sign()){
        ans += '-';
    }
    BigInteger integerPart = numerator / denominator;
    ans += integerPart.toString();
    ans += '.';
    long unsigned int i = 0;
    Rational copy = *this;
    copy.numerator%=copy.denominator;
    while(i < precision) {
        copy.numerator*=10; 
        BigInteger number = copy.numerator / copy.denominator;
        copy.numerator %= copy.denominator;
        ans += (number.if_possitive?number:-1*number).toString();
        i++;
    }
    return ans;
}
Rational::operator double() {
    return std::stod(asDecimal(30));
}
Rational& Rational::operator=(const Rational& n) {
    Rational temp = n;
    swap(temp);
	return *this;
}
void Rational::swap(Rational& n) {
    std::swap(numerator, n.numerator);
    std::swap(denominator, n.denominator);
}
    
std::string Rational::toString() const {
    std::string str;
    BigInteger numCopy = numerator;
    BigInteger denCopy = denominator;
    if (!sign() && numCopy!=0) {
        str += '-';
    }
    numCopy.if_possitive = true;
    denCopy.if_possitive = true;
    str += numCopy.toString();
    if (denCopy != 1) {
        str += '/';
        str += denCopy.toString();
    }
    return str;
}
Rational operator+(const Rational& first, const Rational& second) {
    Rational temp = first;
    temp += second;
    return temp;
}
Rational operator-(const Rational& first, const Rational& second) {
    Rational temp = first;
    temp -= second;
    return temp;
}
Rational operator*(const Rational& first, const Rational& second) {
    Rational temp = first;
    temp *= second;
    return temp;
}
Rational operator/(const Rational& first, const Rational& second) {
    Rational temp = first;
    temp /= second;
    return temp;
}
