#include <iostream>
#include <vector>

struct Node {
    Node(long long val) : value(val), sum(val) {};
    long long value;
    Node* left = nullptr;
    Node* right = nullptr;
    long long sum;
};

class SplayTree {
public:
	void insert(long long value) {
		if (find(value, root)==nullptr) {
			Node* temp = new Node(value);
			std::pair<Node*, Node*> splited = split(value, root);
			root = merge(merge(splited.first, temp), splited.second);
			normalizeSum(root);
		}
	}
	long long sum(long long start, long long end) {
		std::pair<Node*, Node*> firstSplit = split(start-1, root);
		std::pair<Node*, Node*> secondSplit = split(end, firstSplit.second);
		long long ans = (secondSplit.first == nullptr)?0:(secondSplit.first -> sum);
		root = merge(merge(firstSplit.first, secondSplit.first), secondSplit.second);
		return ans;
	}
private:
	Node* root = nullptr;
	void normalizeSum(Node* node) {
		if (node != nullptr) {
			long long l = (node -> left == nullptr)?0:(node -> left -> sum);
			long long r = (node -> right == nullptr)?0:(node -> right -> sum);
			node -> sum = l + r + node -> value;
		}
	}
	Node* zig(Node* node) {
		Node* tempRoot = node -> left;
		node -> left = tempRoot -> right;
		tempRoot -> right = node;
		normalizeSum(node);
		normalizeSum(tempRoot);
		return tempRoot;
	}
	Node* zag(Node* node) {
		Node* tempRoot = node -> right;
		node -> right = tempRoot -> left;
		tempRoot -> left = node;
		normalizeSum(node);
		normalizeSum(tempRoot);
		return tempRoot;
	}
	Node* splay(long long value, Node* node) {
		if (node == nullptr) {
			return nullptr;
		}
		if (value < node -> value) {
			node -> left = splay(value, node->left);
			return zig(node);
		}
		if (node -> value < value) {
			node -> right = splay(value, node->right);
			return zag(node);
		}
		normalizeSum(node);
		return node;
	}
	Node* findMax(Node* node) {
		if (node == nullptr) {
			return nullptr;
		}
		if (node->right == nullptr) {
			return node;
		} else {
			return findMax(node->right);
		}
	}
	Node* merge(Node* firstRoot, Node* secondRoot) {
		Node* temp = findMax(firstRoot);
		if (temp == nullptr) {
			return secondRoot;
		} else {
			firstRoot = splay(temp -> value, firstRoot);
			firstRoot -> right = secondRoot;
			normalizeSum(firstRoot);
			return firstRoot;
		}
	}
	Node* find(long long value, Node* node) {
		if (node == nullptr){
		    	return nullptr;
		}
		if (node -> value == value){
	    		return node;
		}
		if (node -> value > value){
	    		return find(value, node -> left);
		}
		return find(value, node -> right);
	}
	Node* next(long long value, Node* node) {
		if (node == nullptr) {
            		return nullptr;
        	}
        	if (value < node -> value) {
            		Node* left = next(value, node -> left);
            		return left==nullptr?node:left;
        	} else {
            		return next(value, node -> right);
        	}
	}
	std::pair<Node*, Node*> split(long long value, Node* originalRoot) {
		Node* temp = next(value, originalRoot);
		std::pair<Node*, Node*> ans;
		if (temp == nullptr) {
			ans.first = originalRoot;
			ans.second = nullptr;
			return ans;
		} else {
			originalRoot = splay(temp -> value, originalRoot);
			ans.first = originalRoot -> left;
			originalRoot -> left = nullptr;
			normalizeSum(originalRoot);
			ans.second = originalRoot;
			return ans;
		}
	}
};

int main() {
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(0);
	std::cout.tie(0);
	long long n, ans = 0;
	std::cin >> n;
	SplayTree tree;
	for (int i=0; i < n; i++) {
		char sign = 1;
		bool check = false;
		if (sign == '+') {
			check = false;
		} else {
			check = true;
		}
		std::cin >> sign;
		if (sign == '?') {
			long long start, end;
			std::cin >> start >> end;
			ans = tree.sum(start, end);
			std::cout  << ans << '\n';
		}
		if (sign == '+') {
			long long value;
			std::cin  >> value;
			value = check?(value+ans)%1000000000:value;
			tree.insert(value);
			ans = 0;
		}
	}
}
