#include <iostream>
#include <vector>
#include <unordered_map>

class Graph {
public:
    explicit Graph(int n) : graph(n), sz(n){}
    void addEdge(int first, int second) {
        graph[first].push_back(second);
    }
    std::vector<int>& operator[](int i) {
        return graph[i];
    }
    std::vector<int> operator[](int i) const {
        return graph[i];
    }
    int size() const {
        return sz;
    }
private:
    std::vector<std::vector<int>> graph;
    int sz;
};

void dfs(const Graph& graph, int vertice, std::vector<bool>& used, std::vector<int>& start, std::vector<int>& end) {
    static int t = 0;
    start[vertice] = t;
    for (int i : graph[vertice]) {
        if (!used[i]) {
            used[i] = true;
            ++t;
            dfs(graph, i, used, start, end);
        }
    }
    ++t;
    end[vertice] = t;
}

class AncestorFinder {
public:
    AncestorFinder(const Graph& graph, int root) : start(graph.size(), 0), end(graph.size(), 0), used(graph.size(), false) {
    	dfs(graph, root, used, start, end);
    }
    bool isAncestor(int first, int second) {
        return (start[first] < start[second] && end[second] < end[first]);
    }
private:
    std::vector<int> start;
    std::vector<int> end;
    std::vector<bool> used;  
};


int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    int n, root;
    std::cin >> n;
    std::vector<int> vertices(n);
    for (int i = 0; i < n; ++i) {
        std::cin >> vertices[i];
    }
    Graph graph{n};
    for (int i = 0; i < n; ++i) {
        if (vertices[i]!=0) {
            graph.addEdge(vertices[i]-1, i);
        } else {
            root = i;
        }
    }
    AncestorFinder finder{graph, root};
    int m;
    std::cin >> m;
    for (int i = 0; i < m; ++i) {
        int first, second;
        std::cin >> first >> second;
        --first;
        --second;
        if (finder.isAncestor(first, second)) {
            std::cout << 1 << '\n';
        } else {
            std::cout << 0 << '\n';
        }
    }
}

