#include <iostream>
#include <vector>
#include <string>

class HashTable{
public:
    HashTable() : table(SIZE_OF_PRIMES_AND_TABLE, std::vector<hashTableType>(0)), primes(SIZE_OF_PRIMES_AND_TABLE, 1) {
        for (int i = 1; i < SIZE_OF_PRIMES_AND_TABLE; i++) {
            primes[i] = (primes[i-1] * prime) % mod;
        }
    }
    long long find(const std::string& key) const {
        long long hash = getHash(key);
        for (size_t i = 0; i < table[hash].size(); i++) {
            if (table[hash][i].first == key) {
                return table[hash][i].second;
            }
        }
        return 0;
    }
    void add(const std::string& key, const long long value) {
        long long hash = getHash(key);
        bool check = true;
        for (size_t i = 0; i < table[hash].size(); i++) {
            if (table[hash][i].first == key) {
                table[hash][i].second += value;
                check = false;
                break;
            }
        }
        if (check) {
            table[hash].push_back({key, value});
        }
    }
    void erase(const std::string& key) {
        long long hash = getHash(key);
        for (size_t i = 0; i<table[hash].size(); i++) {
            if (table[hash][i].first == key) { 
                table[hash][i].first = ' ';
                table[hash][i].second = 0;
            }
        }
    }
private:
    using hashTableType = std::pair<std::string, long long>;
    std::vector<std::vector<hashTableType>> table;
    const long long prime = 31;
    const long long mod = 1000000007;
    static const int SIZE_OF_PRIMES_AND_TABLE = 100000;
    std::vector<long long> primes;
    
    long long getHash(const std::string& word) const {
        long long ans = 0;
        for (size_t i = 0; i < word.size(); i++) {
            ans = (ans + word[i]*primes[i]) % mod;
        } 
        return (ans % table.size());
    }
};


int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    std::string key;
    HashTable table;
    while(std::cin >> key) {
        long long value;
        std::cin >> value;
        table.add(key, value);
        std:: cout << table.find(key) << '\n';
    }
}
