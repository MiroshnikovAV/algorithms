#include <iostream>
#include <vector>

struct Node {
    Node(long long _begin, long long _end) : begin(_begin), end(_end), square((end-begin)*(end-begin)) {};
    long long begin;
    long long end;
    Node* left = nullptr;
    Node* right = nullptr;
    unsigned char height = 1;
    long long number = 1;
    long long square;
};

class AVLTree {
public:
	void insert(long long begin, long long end){
          	 root = insert(begin, end, root);
    	}
	void erase(long long begin) {
		root = erase(begin, root);
	}
	Node* findByOrder(long long index) {
		if (0 < index <= root->number) {
			Node* ans = findByOrder(index, root);
			return ans;
		} else {
			return nullptr;
		}
	}
	long long sum() {
		return root -> square;
	}
	void bankrupt (long long index) {
		Node* leftNeighbour = findByOrder(index-1);
		Node* bankrupt = findByOrder(index);
		Node* rightNeighbour = findByOrder(index+1);
		if (leftNeighbour == nullptr) {
			long long l = bankrupt->begin;
			long long r = rightNeighbour->end;
			erase(bankrupt->begin);
			erase(rightNeighbour->begin);
			insert(l, r);
		} else if (rightNeighbour == nullptr) {
			long long r = bankrupt->end;
                        long long l = leftNeighbour->begin;
                        erase(bankrupt->begin);
                        erase(l);
                        insert(l, r);
		} else {
			long long l = leftNeighbour->begin;
			long long b = leftNeighbour->end + (bankrupt->end - bankrupt->begin) / 2;
			long long r = rightNeighbour->end;
			erase(rightNeighbour->begin);
			erase(bankrupt->begin);
			erase(l);
			insert(l, b);
			insert(b, r);
		}
	}
	void split(long long index) {
		Node* splited = findByOrder(index);
		long long l = splited->begin;
		long long r = splited->end;
		erase(splited->begin);
		insert(l, l+(r-l)/2);
		insert(l+(r-l)/2, r);
	}
	~AVLTree(){
		eraseTree(root);
	}
private:
	Node* root = nullptr;
	Node* insert(long long begin, long long end, Node* node) {
		if (node == nullptr) {
			return new Node(begin, end);
		}
		if (node->begin > begin) {
			node->left = insert(begin, end, node->left);
		} else {
			node->right = insert(begin, end, node->right);
		}
		return balance(node);
	}
    	
    void normalizeHeight(Node* node) {
        unsigned char l = (node -> left == nullptr)?0:(node -> left -> height);
        unsigned char r = (node -> right == nullptr)?0:(node -> right -> height);
        node -> height = std::max(l, r)+1;
    }
    void normalizeNumber(Node* node) {
	long long l = (node -> left == nullptr)?0:(node -> left -> number);
        long long r = (node -> right == nullptr)?0:(node -> right -> number);
        node -> number = l + r + 1;
    }
    void normalizeSquares(Node* node) {
	long long l = (node -> left == nullptr)?0:(node -> left -> square);
        long long r = (node -> right == nullptr)?0:(node -> right -> square);
        node -> square = l + r + (node -> end - node -> begin)*(node -> end - node -> begin);
    }
    void normalize(Node* node) {
	normalizeHeight(node);
	normalizeNumber(node);
	normalizeSquares(node);
    }
    int checkBalance(Node* node) {
        int l = node -> left == nullptr?0:node->left->height;
        int r = node -> right == nullptr?0:node->right->height;
        return r-l;
    }
    Node* rotateLeft(Node* node) {
        Node* tempRoot = node -> right;
        node -> right = tempRoot -> left;
        tempRoot -> left = node;
        normalize(node);
        normalize(tempRoot);
        return tempRoot;
    }
    Node* rotateRight(Node* node) {
        Node* tempRoot = node -> left;
        node -> left = tempRoot -> right;
        tempRoot -> right = node;
        normalize(node);
        normalize(tempRoot);
        return tempRoot;
    }
    Node* rotateBigLeft(Node* node) {
        node -> right = rotateRight(node -> right);
        return rotateLeft(node);
    }
    Node* rotateBigRight(Node* node) {
        node -> left = rotateLeft(node -> left);
        return rotateRight(node);
    }
    Node* find(long long begin, Node* node) {
        if (node == nullptr){
            return nullptr;
        }
        if (node -> begin == begin){
            return node;
        }
        if (node -> begin > begin){
            return find(begin, node -> left);
        }
	return find(begin, node -> right);
    }
    Node* balance(Node* node) {
	if (node == nullptr) {
		return nullptr;
	}
	normalize(node);
        if (checkBalance(node) == 2) {
            if (checkBalance(node -> right) < 0) {
                return rotateBigLeft(node);
            } else {
                return rotateLeft(node);
            }
        } if (checkBalance(node) == -2) {
            if (checkBalance(node -> left) > 0) {
                return rotateBigRight(node);
            } else {
                return rotateRight(node);
            }
        }
        return node;
    }
    Node* next(long long begin, Node* node) {
        if (node == nullptr) {
            return nullptr;
        }
        if (begin < node -> begin) {
            Node* left = next(begin, node -> left);
            return left==nullptr?node:left;
        } else {
            return next(begin, node -> right);
        }
    }

    Node* eraseMin(Node* node){
        if (node->left!=nullptr){
            node->left = eraseMin(node->left);
            return balance(node);
        } else {
            return node->right;
        }
    }
    Node* erase(long long begin, Node* node){
        if (node == nullptr) {
            return nullptr;
        }
        if (begin < node -> begin){
            node->left = erase(begin, node->left);
            return balance(node);
        }
        if (begin > node -> begin) {
            node->right =  erase(begin, node->right);
            return balance(node);
        }
	Node* right = node -> right;
	Node* left = node -> left;
	if (right == nullptr) {
		return left;
	}
	Node* newRoot = next(node->begin, right);
	delete node;
	newRoot -> right = eraseMin(right);
	newRoot -> left = left;
	return balance(newRoot);
    }
    void eraseTree(Node* node) {
	if(node != nullptr) {
		eraseTree(node->left);
		eraseTree(node->right);
		delete node;
	}
    }
    Node* findByOrder(long long index, Node* node) {
	    if (node == nullptr) {
		return nullptr;
	    }
	    long long nodeNumber = (node -> right == nullptr)?node -> number:(node -> number) - (node -> right -> number);
	    if (nodeNumber == index) {
		return node;
	    }
	    if (index < nodeNumber) {
		return findByOrder(index, node->left);
	    }
	    return findByOrder(index-nodeNumber, node -> right);
    }
};

int main() {
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(0);
	std::cout.tie(0);
	freopen("river.in", "r", stdin);
        freopen("river.out", "w", stdout);
	AVLTree tree;
	int n, p, j = 0;
	std::cin >> n >> p;
	for (int i=0; i < n; i++) {
		long long factory;
		std::cin >> factory;
		tree.insert(j, factory+j);
		j+=factory;
	}
	std::cout << tree.sum() << '\n';
	int k;
	std::cin >> k;
	for (int i=0; i < k; i++) {
		int e; 
		long long u;
		std::cin >> e >> u;
		if (e == 1) {
			tree.bankrupt(u);
		} else {
			tree.split(u);
		}
		std::cout << tree.sum() << '\n';
	}
}
