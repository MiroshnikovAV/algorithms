#include <iostream>
#include <vector>
#include <cmath>

struct Node{
    	Node() = default;
    	Node(int n, int coord) : min(n), minCoord(coord){}
    	int min;
    	int minCoord;
    	int secondStatistic = 0;
};

class SparseTable {
public:
    	SparseTable(const std::vector<int>& array);
    	int findSecondStatistic(int l, int r);
private:
    	std::vector<std::vector<Node>> table;
    	void buildTable(const std::vector<int>& array);
};

void SparseTable::buildTable(const std::vector<int>& array) {
	for (int i=0; i < array.size(); i++) {
    		table[0].push_back(Node(array[i], i));   
 	}
	for(int i = 1; i < table.size()+1; i++) {
    		int j = 0;
    		while (j + pow(2, i) <= array.size()){
			Node temp;
			temp.min = std::min(table[i-1][j].min, table[i-1][j+pow(2,i-1)].min);
			temp.minCoord = (table[i-1][j].min >= table[i-1][j+pow(2,i-1)].min)?table[i-1][j+pow(2,i-1)].minCoord:table[i-1][j].minCoord;
			if(i==1){
	     			temp.secondStatistic = std::max(table[i-1][j].min, table[i-1][j+pow(2,i-1)].min);
			} else {
	    			temp.secondStatistic = std::min(std::max(table[i-1][j].min, table[i-1][j+pow(2,i-1)].min), std::min(table[i-1][j].secondStatistic, table[i-1][j+pow(2,i-1)].secondStatistic));
			}
			table[i].push_back(temp);
			j++;
    		}
	}
}

SparseTable::SparseTable(const std::vector<int>& array) {
	int l = ceil(log(array.size())/log(2));
	table.resize(l, std::vector<Node>());
	buildTable(array);
}

int SparseTable::findSecondStatistic(int l, int r) {
	int j = floor(log(r-l+1)/log(2));
	Node first = table[j][l-1], second = table[j][r-pow(2,j)];
	if (first.minCoord == second.minCoord) {
    		return std::min(first.secondStatistic, second.secondStatistic);
	}
	return std::min(std::max(first.min, second.min), std::min(first.secondStatistic, second.secondStatistic));
}

int main() {
    	std::ios_base::sync_with_stdio(false);
    	std::cin.tie(0);
    	std::cout.tie(0);
    	int n, m;
    	std::cin >> n >> m;
    	std::vector<int> array(n);
    	for(int i = 0; i < n; i++) {
		std::cin >> array[i];
    	}
    	SparseTable s(array);
    	for (int i = 0; i < m; i++) {
		int l, r;
		std::cin >> l >> r;
		std::cout << s.findSecondStatistic(l, r) << '\n';
    	}
}
