// Задача: на подмассиве (начало в l, конец в r) прибавить арифм. прогрессию с первым членом b и разностью d. Всего q таких запросов, в массиве n элементов

#include <iostream>
#include <vector>

int main(){
	int n,q;
	std::cin >> n >> q;
	std::vector<int> array(n);
	for(int i=0; i<n; i++){
		std::cin >> array[i];
	}
	std::vector<int> diff_array(n);
	diff_array[0] = array[0];
	for(int i=0;i<n-1;i++){
		diff_array[i+1] = array[i+1]-array[i];
	}
	std::vector<int> zeroes(n);
	for(int i=0;i<n;i++){
		zeroes[i] = 0;
	}
	for(int i=0; i<q; i++){
		int l,r,b,d;
		std::cin >> l >> r >> b >> d;
		l--;
		for(int j=l;j<r;j++){
			if (j==l){
				zeroes[j]+=b;
			} else {
				zeroes[j]+=d;
			}
		} zeroes[r] -= b+d*(r-l-1);
	}
	int result_element=0;
	for(int i=0;i<n;i++){
		result_element+=diff_array[i]+zeroes[i];
		std::cout << result_element << ' ';
	}
}

