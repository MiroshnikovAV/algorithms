#include <iostream>
#include <vector>

class BinaryIndexedTree{
public:
	BinaryIndexedTree(const std::vector<int>& v);
    	int sum(int i, int j) {
		return sum(j) - sum(i-1);
    	}
	void set(int i, int x);
private:
    	std::vector<int> array;
    	std::vector<int> values;
    	int nextAnd(int i){
		return (i & (i+1));
    	}
    	void change(int i, int d);
    	int sum(int i);
};

BinaryIndexedTree::BinaryIndexedTree(const std::vector<int>& v) : array(v.size(), 0), values(v) {
	for (int i = 0; i < array.size(); i++){
    		change(i, values[i]);
	}
}
void BinaryIndexedTree::change(int i, int d){
	while (i < array.size()) {
    		array[i] +=  d;
    		i = i | (i + 1);
	}
}
void BinaryIndexedTree::set(int i, int x) {
 	int d = x - values[i];
  	values[i] = x;
 	change(i, d);
}
int BinaryIndexedTree::sum(int i) {
	int ans = 0;
	while (i >= 0) {
    		ans += array[i];
		i = nextAnd(i) - 1;
	}
	return ans;
}


int main() {
    	std::ios_base::sync_with_stdio(false);
    	std::cout.tie(0);
    	std::cin.tie(0);
    	int n;
    	std::cin >> n;
    	std::vector<int> even;
    	std::vector<int> odd;
    	for(int i=0; i<n; i++) {
		int temp;
		std::cin >> temp;
		(i%2?odd:even).push_back(temp);
    	}
    	BinaryIndexedTree first(even), second(odd);
    	int m;
    	std::cin >> m;
    	for(int i=0; i<m; i++) {
		int command, x, y;
		std::cin >> command >> x >> y;
	       	if (command) {
	    		std::cout << (first.sum((x)/2, (y-1)/2) - second.sum((x-1)/2, (y-2)/2))*(x%2?1:-1) << '\n';
		} else {
	    		if ((x-1)%2) {
				second.set((x-1)/2, y);
	    		} else {
				first.set((x-1)/2, y);
	    		}
		}
    	}
}
