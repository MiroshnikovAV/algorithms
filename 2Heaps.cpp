#include <iostream>
#include <vector>
#include <string>

struct Heap{
	
	std::vector<std::pair<long long,long long>> values;
	void siftUp(long long i, Heap& heap){
		if (i<=0){
		return; }
		std::pair<long long,long long>& parent = values[(i-1)/2];
		std::pair<long long,long long>& branch = values[i];
		if (parent.first > branch.first){
			std::swap(parent, branch);
			heap.swapIndexes(parent.second, branch.second);
			siftUp((i-1)/2, heap);
		}
	}

	void siftDown(long long i, Heap& heap){
		std::pair<long long,long long>& parent = values[i];
		if ((2*i+2)<values.size()){
			long long j = values[2*i+2].first<values[2*i+1].first;
                	std::pair<long long,long long>& branch = values[2*i+1+j];
                        std::swap(parent, branch);
			heap.swapIndexes(parent.second, branch.second);
                       	siftDown(2*i+1+j, heap);
		} else if (2*i+1<values.size()){
			std::pair<long long,long long>& branch = values[2*i+1];
                  	std::swap(parent, branch);
			heap.swapIndexes(parent.second, branch.second);
        		siftDown(2*i+1, heap);
		}
	}
	void insert(long long x, long long pointer){
		values.push_back(std::pair<long long,long long>(x, pointer));
	}
	std::pair<long long, long long> get(){
		return values[0];
	}
	void extract(Heap& heap){
		values[0].first = 2147483648;
		siftDown(0, heap);
	}
	void makeBig(long long i){
		values[i].first = 2147483648;
	}
	void swapIndexes(long long i, long long j){
		std::swap(values[i].second, values[j].second);
	}
};

class minMaxHeap{
	Heap minHeap, maxHeap;
	long long ptr=0;
public:
	void insert(long long x){
		minHeap.insert(x, ptr);
		maxHeap.insert(-x, ptr);
		minHeap.siftUp(minHeap.values.size()-1, maxHeap);
		maxHeap.siftUp(maxHeap.values.size()-1, minHeap);
		ptr++;
	}
	long long getMin(){
		std::pair<long long, long long> ans = minHeap.get();
		minHeap.extract(maxHeap);
		maxHeap.makeBig(ans.second);
		maxHeap.siftDown(ans.second, minHeap);
		return ans.first;
	}
	long long getMax(){
		std::pair<long long, long long> ans = maxHeap.get();
		maxHeap.extract(minHeap);
		minHeap.makeBig(ans.second);
		minHeap.siftDown(ans.second, maxHeap);
		return -ans.first;
	}
};

int main(){
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(0); 
	std::cout.tie(0); 
	int q;
	std::cin >> q;
	minMaxHeap heap;
	for (int j=0; j<q; ++j){
		std::string command;
		std::cin >> command;
		if (command=="GetMin"){
			std::cout << heap.getMin() << '\n';
		} else if (command=="GetMax"){
			std::cout << heap.getMax() << '\n';
		} else {
			long long value = std::stoll(command.substr(7, command.size()-8));
			heap.insert(value);
		}
	}
}	
