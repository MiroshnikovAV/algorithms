#include <iostream>
#include <vector>

long long Merge(std::vector<long long>& sortedFirst, std::vector<long long>& sortedSecond, std::vector<long long>& array) {
	long long p1=0, p2=0, i=0, inverces = 0, size = array.size();
	std::vector<long long> result(size);
	while (p1 < size / 2 && p2 < (size - size / 2)) {
		if (sortedFirst[p1] < sortedSecond[p2]) {
			result[i] = sortedFirst[p1];
			p1++;
			inverces += p2;
		} else { 
			result[i] = sortedSecond[p2];
			p2++;
	       	}
		i++;
	}
	while (p1 < size / 2) {
		result[i] = sortedFirst[p1];
		p1++;
		i++;
		inverces += p2;
	}
	while (p2 < size - size / 2) {
		result[i] = sortedSecond[p2];
		p2++;
		i++;
	}
	array = result;
	return inverces;
}

long long MergeSort(std::vector<long long>& array){
	long long size = array.size();
	if (size == 1){
		return 0;
	}
	std::vector<long long> firstHalf(size/2);
	std::vector<long long> secondHalf(size - size/2);
	for(int i=0; i < size; i++){
		if (i < size / 2){
			firstHalf[i] = array[i];
		} else {
			secondHalf[i - size / 2] = array[i];
		}
	}
	long long firstInv = MergeSort(firstHalf);
	long long secondInv = MergeSort(secondHalf);

	return Merge(firstHalf, secondHalf, array) + firstInv + secondInv;	
}

int main(){
	freopen("inverse.in", "r", stdin);
	freopen("inverse.out", "w", stdout);
	long long n;
	std::cin >> n;
	std::vector<long long> array(n);
	for(int i=0; i < n; i++){
		std::cin >> array[i];
	}
	long long ans = MergeSort(array);	
	std::cout << ans;
}
