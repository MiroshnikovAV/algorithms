#include <iostream>
#include <vector>
#include <algorithm>
#include <set>

class Graph {
public:
    explicit Graph(int n) : graph(n), sz(n) {}
    std::vector<std::pair<int, int>>& operator[](int i) {
        return graph[i];
    }
    const std::vector<std::pair<int, int>>& operator[](int i) const {
        return graph[i];
    }
    void addEdge(int start, int end, int weight) {
        std::pair<int, int> p = {end, weight};
        graph[start].push_back(p);
    }
    int size() const {
        return sz;
    }
private:
    std::vector<std::vector<std::pair<int, int>>> graph;
    int sz = 0;
};

long long findMinDistance(const Graph& graph, int start, int end) {
    int inf = std::numeric_limits<int>::max();
    int size = graph.size();
    std::set<std::pair<long long, int>> heap;
    std::vector<long long> values(size);
    for (int i = 0; i < size; ++i) {
    	if (i == start){
    	    heap.insert({0, i});
    	    values[i] = 0;
    	} else {
            heap.insert({inf, i});
            values[i] = inf;
        }
    }
    long long ans = -1;
    std::vector<bool> used(size, false);
    while (!heap.empty()) {
        std::pair<long long, int> vertice = *heap.begin();
        if (vertice.second == end) {
            ans = vertice.first;
            break;
        }
        used[vertice.second] = true;
        heap.erase(*heap.begin());
        for (std::pair<int, int> i : graph[vertice.second]) {
            if (!used[i.first]) {
                std::pair<long long, int> temp = *heap.find({values[i.first], i.first});
                temp.first = std::min(temp.first, vertice.first + i.second);
                heap.erase(heap.find({values[i.first], i.first}));
                values[i.first] = temp.first;
                heap.insert(temp);
            }
        }
    } 
    if (ans == inf) {
        return -1;
    }
    return ans;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    int n, m, start, end;
    std::cin >> n >> m >> start >> end;
    Graph graph{n};
    for (int i = 0; i < m; ++i) {
        int first, second, weight;
        std::cin >> first >> second >> weight;
        --first;
        --second;
        graph.addEdge(first, second, weight);
        graph.addEdge(second, first, weight);
    }
    std::cout << findMinDistance(graph, start-1, end-1) << '\n';
}
