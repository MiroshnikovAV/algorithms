#include <iostream>
#include <vector>
#include <string>

class HashTable{
public:
    HashTable() : table(100000, std::vector<std::pair<std::string, std::string>>(0)), primes(20, 1) {
        for (int i = 1; i < 20; i++) {
            primes[i] = (primes[i-1] * prime) % mod;
        }
    }
    std::string find(const std::string& key) {
        long long hash = getHash(key);
        for (unsigned long i = 0; i < table[hash].size(); i++) {
            if (table[hash][i].first == key) {
                return table[hash][i].second;
            }
        }
        return "none";
    }
    void add(const std::string& key, const std::string& value) {
        long long hash = getHash(key);
        bool check = true;
        for (unsigned long i = 0; i < table[hash].size(); i++) {
            if (table[hash][i].first == key) {
                table[hash][i].second = value;
                check = false;
            }
        }
        if (check) {
            table[hash].push_back({key, value});
        }
    }
    void erase(const std::string& key) {
        long long hash = getHash(key);
        for (unsigned long i=0; i<table[hash].size(); i++) {
            if (table[hash][i].first == key) { 
                table[hash][i].first = ' ';
                table[hash][i].second = ' ';
            }
        }
    }
    long long swap(const std::string& firstKey, const std::string& secondKey) {
        std::string firstValue = find(firstKey), secondValue = find(secondKey);
        if (firstValue == "none") {
            firstValue = firstKey;
        }
        if (secondValue == "none") {
            secondValue = secondKey;
        }
        add(firstKey, secondValue);
        add(secondKey, firstValue);
        return abs(std::stoll(firstValue) - std::stoll(secondValue));
    }
private:
    std::vector<std::vector<std::pair<std::string, std::string>>> table;
    const long long prime = 31;
    const long long mod = 1e9+7;
    std::vector<long long> primes;
    
    long long getHash(const std::string& word) {
        long long ans = 0;
        for (unsigned long i = 0; i < word.size(); i++) {
            ans = (ans + word[i]*primes[i]) % mod;
        } 
        return (ans % table.size());
    }
};


int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    std::string line;
    HashTable table;
    int n;
    std::cin >> n;
    for(int i=0; i < n; ++i) {
        std::string firstKey, secondKey;
        std::cin >> firstKey >> secondKey;
        std::cout << table.swap(firstKey, secondKey) << '\n';
    }
}
