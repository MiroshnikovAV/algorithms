#include <iostream>
#include <string>
#include <vector>

void cout(std::vector<std::string>& array, int n){
	for (int i=0; i<n; i++){
                std::cout << array[i];
        }
}

void sort(std::vector<std::string>& array){
	int n = array.size();
	for (int i=0; i<n; i++){
		for (int j=0; j<n; j++){
			if (array[i]+array[j] > array[j]+array[i]){
				std::swap(array[i], array[j]);
			}
		}
	}
}

int main(){
	freopen("number.in", "r", stdin);
	freopen("number.out", "w", stdout);
	std::vector<std::string> array(0);
	std::string s;
	while (std::cin >> s){
		array.push_back(s);
	}
	sort(array);
	cout(array, array.size());
}
