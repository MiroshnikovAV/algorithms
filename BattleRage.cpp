#include <iostream>
#include <vector>

bool if_beatable(std::vector<long long> a, long long q, long long p, long long k){
	long long n = a.size();
	for(int i=0; i<n; i++){
		a[i]-=q*k;
	}
	if (p==q){
		for (int i=0; i<n; i++){
			if (a[i]>0){ return false;}
		}
	} else {
		for (int i=0; i<n; i++){
			if (a[i]>0){
				k-=(a[i]+p-q-1)/(p-q);			
			}
		}
		if (k<0){ return 0;}
	}
	return true;
}

long long count_strikes(std::vector<long long> a, long long q, long long p){
	int l=0, r = 1000000000;
        while (l<r){
                long long middle = (l+r)/2;
                if (if_beatable(a, q, p, middle)){
                        r = middle;
                } else { l = middle+1;}
	}
	return l;
}

int main(){
	std::ios_base::sync_with_stdio(0);
	std::cin.tie(0);
	long long n,p,q;
	std::cin >> n >> p >> q;
	std::vector<long long> a(n);
	for (int i=0; i<n; i++){
		std::cin >> a[i];
	}
	std::cout << count_strikes(a,q,p) << '\n';
}

