#include <iostream>
#include <memory>
#include <vector>

template <size_t T>
class FixedAllocator {
public:
    static FixedAllocator& getAlloc();
    FixedAllocator() = default;
    int8_t* allocate();
    void deallocate(int8_t* pointer);
    ~FixedAllocator();
private:
    const size_t size = 100;
    std::vector<int8_t*> memory;
    std::vector<int8_t*> arrays;
};

template<size_t T>
FixedAllocator<T>& FixedAllocator<T>::getAlloc() {
    static FixedAllocator<T> alloc;
    return alloc;
}

template<size_t T>
FixedAllocator<T>::~FixedAllocator() {
     while(!arrays.empty()) {
        int8_t* pointer = arrays.back();
        arrays.pop_back();
        delete[] pointer;
    }
}

template<size_t T>
int8_t* FixedAllocator<T>::allocate() {
    if (memory.empty()) {
        int8_t* array = new int8_t[size * T];
        for (size_t i = 0; i < size * T; i += T) {
            memory.push_back(array + i);
        }
        arrays.push_back(array);
    }
    int8_t* pointer = memory.back();
    memory.pop_back();
    return pointer;
}

template<size_t T>
void FixedAllocator<T>::deallocate(int8_t* pointer) {
    memory.push_back(pointer);
}

template <typename T>
class FastAllocator {
public:
    using value_type = T;
    template <typename U>
    struct rebind {
        using other = FastAllocator<U>;
    };
    FastAllocator() = default;
    template <typename U>
    FastAllocator(const FastAllocator<U>&) {}
    ~FastAllocator() = default;

    T* allocate(int n);
    void deallocate(T* pointer, int n);
};

template<typename T>
T *FastAllocator<T>::allocate(int n) {
    if (n == 1 && (sizeof(T) == 24)) {
        T* t = reinterpret_cast<T*>(FixedAllocator<sizeof(T)>::getAlloc().allocate());
        return t;
    } else {
        T* t = reinterpret_cast<T*>(operator new(sizeof(T)*n));
        return t;
    }
}

template<typename T>
void FastAllocator<T>::deallocate(T *pointer, int n) {
    if (n == 1 && (sizeof(T) == 24)) {
        FixedAllocator<sizeof(T)>::getAlloc().deallocate(reinterpret_cast<int8_t*>(pointer));
    } else {
        delete(reinterpret_cast<int8_t*>(pointer));
    }
}


template <typename T, typename Allocator = std::allocator<T>>
class List {
private:
    template <bool isConst>
    class Iterator;
public:
    using iterator = Iterator<false>;
    using const_iterator = Iterator<true>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    explicit List(const Allocator& _alloc = Allocator());
    List(size_t count, const T& value, const Allocator& _alloc = Allocator());
    List(size_t count, const Allocator& _alloc = Allocator());
    List(const List& list);
    ~List();
    List& operator=(const List& list);
    size_t size() const;
    Allocator get_allocator() const;
    iterator begin();
    const_iterator begin() const;
    iterator end();
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;
    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;

    void insert(const_iterator it, const T& value);
    void erase(const_iterator it);
    void push_back(const T& value);
    void push_front(const T& value);
    void pop_back();
    void pop_front();
private:
    struct Node {
        Node() = default;
        Node(const T& v);
        T value;
        Node* prev = nullptr;
        Node* next = nullptr;
    };

    Node* nullNode;
    size_t sz = 0;

    using NodeAlloc = typename std::allocator_traits<Allocator>::template rebind_alloc<Node>;
    using Traits = std::allocator_traits<NodeAlloc>;

    NodeAlloc alloc;

    template <bool isConst>
    class Iterator {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = typename std::conditional<isConst, const T, T>::type;
        using pointer = typename std::conditional<isConst, const T*, T*>::type;
        using reference = typename std::conditional<isConst, const T&, T&>::type;
        using iterator_category = std::bidirectional_iterator_tag;
        friend Iterator<not isConst>;
        friend List;

        explicit Iterator(Node* node);
        Iterator(const Iterator& it);
        Iterator& operator++();
        Iterator operator++(int);
        Iterator& operator--();
        Iterator operator--(int);
        template <bool isConst2>
        bool operator==(const Iterator<isConst2>& it) const;
        template <bool isConst2>
        bool operator!=(const Iterator<isConst2>& it) const;
        std::conditional_t<isConst, const T&, T&> operator*() const;
        std::conditional_t<isConst, const T*, T*> operator->() const;
        operator Iterator<true>() const;
    private:
        Node* current;
    };
};

template<typename T, typename Allocator>
template<bool isConst>
List<T, Allocator>::Iterator<isConst>::Iterator(List::Node* node) {
    current = node;
}

template<typename T, typename Allocator>
template<bool isConst>
List<T, Allocator>::Iterator<isConst>::Iterator(const List<T, Allocator>::Iterator<isConst>& it) {
    current = it.current;
}

template<typename T, typename Allocator>
template<bool isConst>
typename List<T, Allocator>::template Iterator<isConst>& List<T, Allocator>::Iterator<isConst>::operator++() {
    current = current->next;
    return *this;
}

template<typename T, typename Allocator>
template<bool isConst>
typename List<T, Allocator>::template Iterator<isConst> List<T, Allocator>::Iterator<isConst>::operator++(int) {
    Iterator temp = *this;
    current = current->next;
    return temp;
}

template<typename T, typename Allocator>
template<bool isConst>
typename List<T, Allocator>::template Iterator<isConst>& List<T, Allocator>::Iterator<isConst>::operator--() {
    current = current->prev;
    return *this;
}

template<typename T, typename Allocator>
template<bool isConst>
typename List<T, Allocator>::template Iterator<isConst> List<T, Allocator>::Iterator<isConst>::operator--(int) {
    Iterator temp = *this;
    current = current->prev;
    return temp;
}

template<typename T, typename Allocator>
template<bool isConst>
template<bool isConst2>
bool List<T, Allocator>::Iterator<isConst>::operator==(const List::Iterator<isConst2>& it) const {
    return current == (it.current);
}

template<typename T, typename Allocator>
template<bool isConst>
template<bool isConst2>
bool List<T, Allocator>::Iterator<isConst>::operator!=(const List::Iterator<isConst2>& it) const {
    return current != (it.current);
}

template<typename T, typename Allocator>
template<bool isConst>
std::conditional_t<isConst, const T&, T&> List<T, Allocator>::Iterator<isConst>::operator*() const {
    return current->value;
}

template<typename T, typename Allocator>
template<bool isConst>
std::conditional_t<isConst, const T*, T*> List<T, Allocator>::Iterator<isConst>::operator->() const {
    return &current->value;
}

template<typename T, typename Allocator>
template<bool isConst>
List<T, Allocator>::Iterator<isConst>::operator Iterator<true>() const {
    return Iterator<true>(current);
}

template<typename T, typename Allocator>
List<T, Allocator>::Node::Node(const T& v) : value(v) {}

template<typename T, typename Allocator>
List<T, Allocator>::List(const Allocator& _alloc) : sz(0), alloc(_alloc) {
    nullNode = Traits::allocate(alloc, 1);
    nullNode->next = nullNode;
    nullNode->prev = nullNode;
}

template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const T& value, const Allocator& _alloc) : sz(0), alloc(_alloc) {
    nullNode = Traits::allocate(alloc, 1);
    nullNode->next = nullNode;
    nullNode->prev = nullNode;
    for (size_t i = 0; i < count; i++) {
        push_back(value);
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const Allocator& _alloc) : sz(0), alloc(_alloc) {
    nullNode = Traits::allocate(alloc, 1);
    nullNode->next = nullNode;
    nullNode->prev = nullNode;
    for (size_t i = 0; i < count; i++) {
        Node* prev = nullNode -> prev;
        Node* newNode = Traits::allocate(alloc, 1);
        Traits::construct(alloc, newNode);
        prev->next = newNode;
        nullNode->prev = newNode;
        newNode->next = nullNode;
        newNode->prev = prev;
        ++sz;
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(const List& list) : sz(0) {
    alloc = Traits::select_on_container_copy_construction(list.alloc);
    nullNode = Traits::allocate(alloc, 1);
    nullNode->next = nullNode;
    nullNode->prev = nullNode;
    List::const_iterator it = list.begin();
    while (it != list.end()) {
        push_back(*it);
        ++it;
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::~List() {
    while (sz > 0) {
        pop_back();
    }
    Traits::deallocate(alloc, nullNode, 1);
}

template<typename T, typename Allocator>
List<T, Allocator>& List<T, Allocator>::operator=(const List& list) {
    if (Traits::propagate_on_container_copy_assignment::value) {
        alloc = list.alloc;
    }
    while(sz > list.size()) {
        pop_back();
    }
    List::iterator first = begin();
    List::const_iterator second = list.begin();
    for (size_t i = 0; i < sz; ++i) {
        (*first) = (*second);
        ++first;
        ++second;
    }
    while(second != list.end()) {
        push_back(*second);
        ++second;
    }
    return *this;
}

template<typename T, typename Allocator>
size_t List<T, Allocator>::size() const {
    return sz;
}

template<typename T, typename Allocator>
Allocator List<T, Allocator>::get_allocator() const {
    return alloc;
}

template<typename T, typename Allocator>
typename List<T, Allocator>::iterator List<T, Allocator>::begin() {
    return iterator(nullNode->next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::begin() const {
    return cbegin();
}

template<typename T, typename Allocator>
typename List<T, Allocator>::iterator List<T, Allocator>::end() {
    return iterator(nullNode);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::end() const {
    return cend();
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::cbegin() const {
    return const_iterator(nullNode->next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::cend() const {
    return const_iterator(nullNode);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::reverse_iterator List<T, Allocator>::rbegin() {
    return std::make_reverse_iterator(end());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::reverse_iterator List<T, Allocator>::rend() {
    return std::make_reverse_iterator(begin());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::rbegin() const {
    return crbegin();
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::rend() const {
    return crend();
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::crbegin() const {
    return std::make_reverse_iterator(cend());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::crend() const {
    return std::make_reverse_iterator(cbegin());
}

template<typename T, typename Allocator>
void List<T, Allocator>::insert(List::const_iterator it, const T& value) {
    Node* prev = it.current -> prev;
    Node* newNode = Traits::allocate(alloc, 1);
    Traits::construct(alloc, newNode, value);
    prev->next = newNode;
    it.current->prev = newNode;
    newNode->next = it.current;
    newNode->prev = prev;
    ++sz;
}

template<typename T, typename Allocator>
void List<T, Allocator>::erase(List::const_iterator it) {
    Node* next = it.current->next;
    Node* prev = it.current->prev;
    prev->next = next;
    next->prev = prev;
    Traits::destroy(alloc, it.current);
    Traits::deallocate(alloc, it.current, 1);
    --sz;
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_back(const T& value) {
    insert(end(), value);
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_front(const T& value) {
    insert(begin(), value);
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_back() {
    erase(--end());
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_front() {
    erase(begin());
}


