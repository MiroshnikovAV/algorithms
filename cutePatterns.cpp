#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

bool check(int mask, int nextMask, int n) {
    for (int i = 0; i < n-1; i++) {
        if (((mask & 1) + ((mask >> 1) & 1) + (nextMask & 1) + ((nextMask >> 1) & 1)) % 4 == 0) {
            return false;
        }
        mask >>= 1;
        nextMask >>= 1;
    }
    return true;
}

int cutePatternsCounter(int m, int n) {
    std::vector<std::vector<int>> dp(m, std::vector<int>(pow(2, n), 0));
    for (int i = 0; i < pow(2, n); ++i) {
        dp[0][i] = 1;
    }
    for (int i = 0; i < m-1; i++) {
        for (int mask = 0; mask < pow(2, n); ++mask) {
            for (int nextMask = 0; nextMask < pow(2,n); ++nextMask) {
                if (check(mask, nextMask, n)) {
                    dp[i+1][nextMask] += dp[i][mask];
                }
            }
        }
    }
    int ans = 0;
    for (int i = 0; i < pow(2,n); ++i) {
        ans += dp[m-1][i];
    }
    return ans;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    int m, n;
    std::cin >> m >> n;
    if (m < n) {
        std::swap(n, m);
    }
    
    std::cout << cutePatternsCounter(m, n);
}
