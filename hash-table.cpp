#include <iostream>
#include <vector>
#include <string>

class HashTable{
public:
    HashTable() : table(100000, std::vector<std::pair<std::string, std::string>>(0)), primes(20, 1) {
        for (int i = 1; i < 20; i++) {
            primes[i] = (primes[i-1] * prime) % mod;
        }
    }
    std::string find(const std::string& key) const {
        long long hash = getHash(key);
        for (unsigned long i = 0; i < table[hash].size(); i++) {
            if (table[hash][i].first == key) {
                return table[hash][i].second;
            }
        }
        return "none";
    }
    void add(const std::string& key, const std::string& value) {
        long long hash = getHash(key);
        bool check = true;
        for (unsigned long i = 0; i < table[hash].size(); i++) {
            if (table[hash][i].first == key) {
                table[hash][i].second = value;
                check = false;
            }
        }
        if (check) {
            table[hash].push_back({key, value});
        }
    }
    void erase(const std::string& key) {
        long long hash = getHash(key);
        for (unsigned long i=0; i<table[hash].size(); i++) {
            if (table[hash][i].first == key) { 
                table[hash][i].first = ' ';
                table[hash][i].second = ' ';
            }
        }
    }
private:
    std::vector<std::vector<std::pair<std::string, std::string>>> table;
    const long long prime = 31;
    const long long mod = 1e9+7;
    std::vector<long long> primes;
    
    long long getHash(const std::string& word) const {
        long long ans = 0;
        for (unsigned long i = 0; i < word.size(); i++) {
            ans = (ans + word[i]*primes[i]) % mod;
        } 
        return (ans % table.size());
    }
};


int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    freopen("map.in", "r", stdin);
    freopen("map.out", "w", stdout);
    std::string line;
    HashTable table;
    while(std::cin >> line) {
        if (line == "put") {
            std::string key, value;
            std::cin >> key >> value;
            table.add(key,value);
        } else if (line == "get") {
            std::string key;
            std::cin >> key;
            std::cout << table.find(key) << '\n';
        } else if (line == "delete") {
            std::string key;
            std::cin >> key;
            table.erase(key);
        }
    }
}
