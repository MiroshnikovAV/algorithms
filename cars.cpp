#include <iostream>
#include <vector>
#include <set>
#include <map>

int main(){
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(0);
	std::cout.tie(0);
	int n, k, p;
	long long inf = 9223372036854775807;
	std::cin >> n >> k >> p;
	std::vector<long long> query(p);
	std::map<long long, long long> r;
	for(int i=0; i<p; i++) {
		long long t;
		std::cin >> t;
		query[i]=t;
		r[t] = inf;
	}
	std::vector<long long> future(p);
	for(int i=p-1; i>=0; i--) {
		future[i] = r[query[i]];
		r[query[i]] = i;
	}
	std::set<std::pair<long long, long long>> set;
	long long ans = 0;
	for(int i=0; i<p; i++) {
		if (set.find({i, query[i]}) == set.end()) {
			ans++;
			if (set.size() == k) {
					set.erase(*set.rbegin());
			}
			set.insert({future[i], query[i]});
		} else {
			set.erase({i, query[i]});
			set.insert({future[i], query[i]});
		}
	}
	std::cout << ans;
}
